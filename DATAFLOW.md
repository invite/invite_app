# Image Processing Data Flow
After the camera is opened [DepthHandler.java](TraitDetector/app/src/main/java/com/wur/invite/traitdetector/depth/DepthHandler.java) actiates ARCore and starts streaming from the camera. Everytime a frame is drawn (`onDrawFrame` based on the ARCore Getting Started application) there is a check whether the button for capturing a photo is pressed. 

## Summary
The model creates a folder in the `Gallery` for each variety using the format `<year>_<trial id>_<variety id>`. Within this folder the raw image is stored as well as the individual masks as extracted by the model. An example of a trial performed in year `2023`, trial `1`, and variety `1` can be found in the [sample](sample) folder.

The variety data that is calculated is stored in the `trialData.json` file. This `json` file contains an array with all the individual trials, with average scores for each individual traits. The final output after hitting the "snapshot" button is:

```json
"traits": {
    "27":{"score":0,"value":0.8279384255409241},
    "31":{"score":0,"value":1503.8},
    "34":{"score":0,"value":73.20500335693359},
    "101":{"score":0,"value":60.49634323120117},
    "102":{"score":0,"value":1355690.838366231},
    ...
}
```

For more information on how the output is generated look below.


## Capture Image
Once the snapshot button is pressed, the Image Processing Data Flow is activated by calling `depthTexture.saveDepth()`


```java
... 
if (capturePicture && distance != 0) {
    ...

    JSONObject traits = depthTexture.saveDepth(this, frame, qrCodeValues, cameraImage, pixelStride, rowStride, depthBuffer, depthShape);
    ...
}
...
```

This function takes the following inputs:

 frame
Is updated every frame update though the sessions of ARCore:
```java
Frame frame = session.update();
```

### qrCodeValues
Values are scanned during the session or are initiated with dummy data

```java
...
private QrCodeHandler qrCodeHandler = new QrCodeHandler();
...
if (qrEnabled) {
    if (!qrCodeRetrieved) {
        message += "\nNo QR code found. Please try moving the camera slightly.";

        qrCodeValues = qrCodeHandler.analyze(cameraImage);

        if (qrCodeValues.length() != 0) {
            qrCodeRetrieved = true;
        }
    } 
} else {
    message += "\nQR scanning is disabled. Now using default values.";
    qrCodeValues = qrCodeHandler.defaultValues();
}
```

### cameraImage
Is updated every frame update 
```java
Image cameraImage = frame.acquireCameraImage();
```

### pixelStride, rowStride, depthBuffer, depthShape
The `pixelstride` depends on the camera chip itself and differs between phones. You will need to scale the `depthImage` to the `cameraImage` in order to get a depth value for each available pixel. The `depthBuffer` contains a `16 bit` buffer with all the depth information as returned by ARCore. Since it is a `16 bit` buffer, the `pixelStride` is always 2. Finally the depthShape returns the shape of the `depthImage` in order to scale the pixels when determining the distance.

```java
...
Image.Plane depthPlane = depthImage.getPlanes()[0];

int pixelStride = depthPlane.getPixelStride();
int rowStride = depthPlane.getRowStride();
ByteBuffer depthBuffer = depthPlane.getBuffer().order(ByteOrder.nativeOrder());

int[] depthShape = {depthImage.getWidth(), depthImage.getHeight()};
...
```

## Saving Depth
`saveDepth` works with a couple of phases. These are explained below:

### Get Field of View
The field of view (fov) is used to determine the conversion from pixels to millimeters. Using the intrinsics of the camera the fov for the width and height can be calculted based on a standard calculation.

```java
..
Camera camera = frame.getCamera();
CameraIntrinsics imageIntrinsics = camera.getImageIntrinsics();
float[] focalLength = imageIntrinsics.getFocalLength();
int[] imageDimensions = imageIntrinsics.getImageDimensions();
double fovW = 2 * Math.atan(imageDimensions[0] / (focalLength[0] * 2.0));
double fovH = 2 * Math.atan(imageDimensions[1] / (focalLength[0] * 2.0));
float[] fov = {(float) fovW, (float) fovH};
..
```

### Trial Info
Using the `qrData` a folder is created for the variety where image and masks will be stored. 
```java
...
String year = qrData.getString("year");
String trial = qrData.getString("trial");
String id = qrData.getString("id");
traits = qrData.getJSONObject("traits");
String varietyCode = year + "_" + trial + "_" + id;
String picturesPath = Environment.getExternalStoragePublicDirectory(
        Environment.DIRECTORY_PICTURES).toString();
File trialDataPath = new File(picturesPath + "/Invite/" + varietyCode);
...
```

An example of a trial performed in year `2023`, trial `1`, and variety `1` can be found in the [sample](sample) folder.


### Save Image
The raw camera image will be saved in the `trialDataPath` as described above. 

```java
...
if (!trialDataPath.exists()) {
    trialDataPath.mkdirs();
}
final File out = new File(trialDataPath.getAbsolutePath(), "raw.png");
...
```

Lines [127](TraitDetector/app/src/main/java/com/wur/invite/traitdetector/depth/DepthHandler.java#L127) to [162](TraitDetector/app/src/main/java/com/wur/invite/traitdetector/depth/DepthHandler.java#L162) handle parsing and saving the image to the internal storage.

### Process Image
Once the raw camera image is secured the image needs to be processed. This is where the model deeloped by Dan will take over. `Processor.full_process_image(img, user_cfg)` is called using the raw camera image that was stored above and with a `user_cfg` as is defined in a [Config.java](TraitDetector/app/src/main/java/com/wur/invite/traitdetector/config/Config.java) file. This file will be different for each fruit/vegetable and described the model specific meta data. Processing follows the following steps:

1.  **Detect Instances**  
    The image is processed using `YOLOv8.process_image(img)`. The Yolo model is called using `model.session.run(input_container)` to generate `results` and `predictions`.

    These are processed using `process_box_and_mask_output(img, predictions1, predictions2);` and returns a `List` of `Instances`. These `Instances` are not returned to the user but are used to calculate the traits. Each prediction and `Instance` is created using 

    ```java
    ...
    Instance instance = new Instance(c, b, s, m);
    ...
    ```
    
    where `c` is the class, `b` is an array with boundingbox (x1, y1,x2,y2), `s` is the score (certainty) and `m` is a float array with the mask. A series of calculations generates a `mask_map` which will be added to the `Instance` in the end as `m_map`.

    Each instance is added to the `outputs`.

2.  **Detect Peduncle**  
    A secondary model determines the peduncle scar, for this optional upscaling is used. All tomato masks are upscaled using super resoluation if necessary, but for the ones facing up there are also ran through the secondary detection model for the peduncle (**NOTE:** Currently all tomatos are treated as facing up, this needs to be fixed).

    Once a peduncle is detected the `Instance` is appended with information on the peduncle in a similar manner as sescribed above:

    ```java
    ...
    tomato_instance.c2 = c2;
    tomato_instance.s2 = s2;
    tomato_instance.m_map2 = clone_mat;
    tomato_instance.b2 = b2;
    ...
    ```

3.  **Convert to Traits**
    Using the information extracted from the model above the traits can be determined. For each returned `Instance` the traits are calculated by calling using some of the parameters that were determined before.

    ```java
    ...
    Processor.instances_to_traits(data_list, pixelStride, rowStride, depthBuffer, fov, img_shape, depth_shape);
    ...
    ```

    This function appends all instances with data about the traits and transforms the `Instances` into a `List` of `Traits`. `Traits` are aggregated results of all `Instances`, how each `Trait` is calculated depends on the what it is that is being scored, but it will always be the trait id as key and the `score` and a calculated `value` as the value:

    ```json
    "27":{"score":0,"value":0.8279384255409241},
    ```

4.  **Save Masks and Return Trial Data**
    Finally all indiviual `Instance` masks are saved in the `trialDataPath` under the `masks` folder. a sample of the output can be seen in the [sample](sample/2023_1_1/masks) folder.

    Finally the traits from this specific variety are returned and stored in the `trialData.json` file keeping track of all the varities within the trial. This file is appended with all the relevant data.

    ```json
    {
        "year":"2023",
        "trial":"1",
        "id":1,
        "label":"1",
        "traits": {
            "27":{"score":0,"value":0.8279384255409241},
            "31":{"score":0,"value":1503.8},
            "34":{"score":0,"value":73.20500335693359},
            "101":{"score":0,"value":60.49634323120117},
            "102":{"score":0,"value":1355690.838366231}
        },
        "distance":100.4
    }
    ```


