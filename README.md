# INVITE Trait Detector APP

## TODO list

#### Must Have
* Data analytics (std, max, min, ave) [GUIDO]
* Distance confidence in json and on screen (red, orange, green) [GUIDO]
* Gallery and trial data management [GUIDO]
* Remove diameter [DAN]

#### Should Have
* Verify Diameter Calculation [GUIDO]
* Improved volume estimation 2nd iteration [GUIDO]
* FOV debugging [GUIDO]
* Add traits (as long as feasible)
    * Ribbing [DAN]
    * Longitudinal shape [DAN]
    * Blossom End Scar [DAN]

#### Could Have
* Add traits
    * Color [DAN]
* Tap zoom on tomato to get individual data  [DAN]
* Lower the versioning; try on other phones [DAN/GUIDO]
* Get depth map [GUIDO]
* Store depth map [GUIDO]
* Add settings (threshold, etc.) [DAN]
* Delete Photo [DAN/GUIDO]
* Automatic scoring after taking picture (only works after enough references) [GUIDO]
* QR based on Open CV, without active scanning [DAN]

#### Won't Have
* Peduncle depression
* Account management
* Data storage
* iOS translation
