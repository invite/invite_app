package com.wur.invite.traitdetector.others;

import com.wur.invite.traitdetector.processing.Instance;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;

public class Result {
    List<Instance> instances;
    float diameter;
    float height;
    float volume;
    float ratio;

    public Result(List<Instance> instances, float diameter, float height, float volume, float ratio) {
        this.instances = instances;
        this.diameter = diameter;
        this.height = height;
        this.volume = volume;
        this.ratio = ratio;
    }

    public List<Instance> getInstances() {
        return instances;
    }

    public float getDiameter() {
        return diameter;
    }

    public float getHeight() {
        return height;
    }

    public float getVolume() {
        return volume;
    }

    public float getRatio() {
        return ratio;
    }
}
