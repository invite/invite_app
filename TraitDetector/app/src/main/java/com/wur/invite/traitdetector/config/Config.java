package com.wur.invite.traitdetector.config;

public class Config {

    // Display config
    public int draw_contours = 1;
    public int draw_labels = 1;
    public int draw_boxes = 0;
    public int[][] colors = {{255, 255, 255}, {255, 0, 0}, {125, 125, 125}};
    public int thickness = 2;
    public float font_scale = 1.6f;
    public int show_orientation = 0; // Great for debugging

    // General config
    // 1 = YOLO; no peduncles
    // 2 = YOLO + SR + YOLO
    public int pipeline = 2;



    // Mask RCNN
    public String maskrcnn_model_filename = "maskrcnn-1scale.ptl";

    // YOLO
    public String yolo_model_filename = "yolov8n-seg-320.onnx"; // 335 ms
    public String[] yolo_classes_list = {"tomato"};
    public float yolo_score_threshold = 0.25f;
    public float yolo_iou_threshold = 0.5f;
    public int yolo_input_size = 320;
    public float yolo_box_padding = 1.0f;



    // SRGAN
    public String sr_model_filename = "realesrgan-x2.ort"; // 0.98s each
    public int sr_threshold = 32;           // Threshold if SR will be used
    public int sr_input_size = 64;          // Required input size
    public int sr_output_size = 128;        // x2
    public int sr_force_size = 320;         // Forced input size for 2nd YOLO



    // YOLO 2
    public String yolo_model_filename_2 = "yolov8n-seg-320-2.onnx"; // 292 ms
    public String[] yolo_classes_list_2 = {"tomato", "peduncle"};
    public float yolo_score_threshold_2 = 0.5f;
    public float yolo_iou_threshold_2 = 0.5f;
    public int yolo_input_size_2 = 320;
    public float yolo_box_padding_2 = 1.0f;















    // Deeplab
    public String deeplab_model_filename = "deeplabv3.onnx";
    public int deeplab_input_size = 512;
    public int deeplab_output_size = 512;

}
