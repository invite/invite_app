package com.wur.invite.traitdetector.gallery;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wur.invite.traitdetector.R;

import java.util.ArrayList;

public class CroppedAdapter extends RecyclerView.Adapter<CroppedAdapter.CroppedViewHolder> {
    private ArrayList<Cropped> cropped;

    public CroppedAdapter(ArrayList<Cropped> cropped) {
        this.cropped = cropped;
    }

    @NonNull
    @Override
    public CroppedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cropped_view, parent, false);

        return new CroppedViewHolder(v);
    }

    public void onBindViewHolder(@NonNull CroppedViewHolder holder, int position) {
        Cropped crop = cropped.get(position);

        Drawable d = Drawable.createFromPath(crop.getImageURI());
        holder.image.setImageDrawable(d);
    }

    @Override
    public int getItemCount() {
       if (cropped != null) {
           return cropped.size();
       } else {
           return 0;
       }
    }

    public static class CroppedViewHolder extends RecyclerView.ViewHolder {
        public final View view;
        public final ImageView image;

        public CroppedViewHolder(View view) {
            super(view);

            this.view = view;
            image = view.findViewById(R.id.croppedImage);
        }
    }
}

