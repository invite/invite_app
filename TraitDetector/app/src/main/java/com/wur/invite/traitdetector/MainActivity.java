//
// Generic Mask-RCNN Android APP
// Authored by: DJAR
// Wageningen University and Research
// Greenhouse Technology Business Unit
//
// 2022_09_06
// - Initial commit
// - Real time detection does not work yet (since there are no samples)
// - A little offset in the output results
//

package com.wur.invite.traitdetector;

import static com.wur.invite.traitdetector.trials.Trials.initTrialData;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.soloader.nativeloader.NativeLoader;
import com.facebook.soloader.nativeloader.SystemDelegate;
import com.wur.invite.traitdetector.common.FileFinder;
import com.wur.invite.traitdetector.config.Config;
import com.wur.invite.traitdetector.depth.ARActivity;
import com.wur.invite.traitdetector.gallery.TomatoActivity;
import com.wur.invite.traitdetector.gallery.TouchHandler;
import com.wur.invite.traitdetector.gallery.Tomato;
import com.wur.invite.traitdetector.gallery.TomatoAdapter;
import com.wur.invite.traitdetector.depth.DepthData;
import com.wur.invite.traitdetector.processing.ImageOps;
import com.wur.invite.traitdetector.processing.Instance;
import com.wur.invite.traitdetector.processing.Processor;
import com.wur.invite.traitdetector.processing.models.SRGan;
import com.wur.invite.traitdetector.processing.models.YOLOv8;
import com.wur.invite.traitdetector.processing.models.YOLOv8_2;
import com.wur.invite.traitdetector.trials.Trials;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView tomatoes;
    private RecyclerView.Adapter adapter;
    private ArrayList<Tomato> tomatoList;
    private JSONArray trialData;

    static {
        if (!NativeLoader.isInitialized()) {
            NativeLoader.init(new SystemDelegate());
        }
        if (OpenCVLoader.initDebug()) {
            Log.d("D2go", "Successful OpenCV configuration");
        } else {
            Log.d("D2go", "Failed OpenCV configuration");
        }
//        NativeLoader.loadLibrary("torchvision_ops");
    }



    int index = 0;
    private final String imagePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/Invite";
    File imageDirectory = new File(imagePath);
    private String[] mTestImages = {"demo0.png", "demo1.png", "demo2.png", "demo3.png", "demo4.png"};
    float[] fov = {(float) 1.0962085, (float) 0.66157705}; // Hardcoded for galaxy a52
    public Mat open_image_file(String file_name){
        String image_filename = null;
        try {
            image_filename = FileFinder.assetFilePath(getApplicationContext(), file_name);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Mat img;
        img = Imgcodecs.imread(image_filename, Imgcodecs.IMREAD_COLOR);
        return img;
    }











    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        try {
            this.tomatoList = initTrialData(getApplicationContext());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        this.tomatoes = findViewById(R.id.mediaView);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
        this.tomatoes.setLayoutManager(mLayoutManager);
        adapter = new TomatoAdapter(this.tomatoList);
        this.tomatoes.setAdapter(adapter);


        //
        // Load model config
        //
        Config user_cfg = new Config();

        // STAGE 1: YOLO Tomato detector
        String yolo_model_filename = "";
        try {
            yolo_model_filename = FileFinder.assetFilePath(getApplicationContext(), user_cfg.yolo_model_filename);
            Log.d("INVITE", yolo_model_filename);
            if (yolo_model_filename.contains("yolo")) {
                YOLOv8.initModel(getApplicationContext(), user_cfg);
            }
        } catch (IOException e) {
            Log.d("INVITE", "ERROR!!!!");
            e.printStackTrace();
        }


        // STAGE 2: Tomato SR
        SRGan.initModel(getApplicationContext(), user_cfg);


        // STAGE 3: YOLO Tomato peduncle detector and mask optimizer
        String yolo_model_filename_2 = null;
        try {
            yolo_model_filename_2 = FileFinder.assetFilePath(getApplicationContext(), user_cfg.yolo_model_filename_2);
            Log.d("INVITE", yolo_model_filename_2);
            if (yolo_model_filename_2.contains("yolo")) {
                YOLOv8_2.initModel(getApplicationContext(), user_cfg);
            }
        } catch (IOException e) {

            Log.d("INVITE", "ERROR!!!!");
            e.printStackTrace();
        }


        //
        // Set layout variables
        //
        ImageView imageView = findViewById(R.id.imageView);
        final Button buttonSelect = findViewById(R.id.selectButton);
        final Button buttonExport = findViewById(R.id.exportButton);
        final Button buttonTest = findViewById(R.id.testButton);
        final CheckBox qrCheckBox = findViewById(R.id.qrEnabledBox);

        this.tomatoes.addOnItemTouchListener(
                new TouchHandler(this, this.tomatoes, new TouchHandler.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // do whatever
                        Log.d("RECYCLER", "TOUCH");
                        trialData = Trials.loadTrialData(getApplicationContext());
                        Intent intent = new Intent(MainActivity.this, TomatoActivity.class);
                        try {
                            JSONObject variety = trialData.getJSONObject(position);
                            intent.putExtra("variety", variety.toString());
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        Log.d("RECYCLER", "LONG TOUCH");
                    }
                })
        );

        // Take image
        buttonSelect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent takePicture = new Intent(MainActivity.this, ARActivity.class);
                takePicture.putExtra("qr_enabled", qrCheckBox.isChecked());
                startActivityForResult(takePicture, 0);
            }
        });

        // Export
        buttonExport.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //
                // Remove any existing CSV and create an empty new file
                //

                File csvFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "trialData.csv");
                if (csvFile.exists()) {
                    csvFile.delete();
                }

                try {
                    csvFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //
                // Generate list for the column names; Create an empty JSON for traits scores and load the Trial Data
                //

                List<String> columns = new ArrayList<String>();
                columns.add("year");
                columns.add("date");
                columns.add("trial");
                columns.add("id");

                JSONObject traitScores = new JSONObject();
                JSONArray trialData = Trials.loadTrialData(getApplicationContext());
//            Trait map:  26 Size
//                        27 Ratio
//                        28 Shape
//                        31 Peduncle Scar Size
//                        33 Blossom End Shape
//                        34 Diameter
//                        101 Height
//                        102 Volume


                //
                // Start by extracting all the scores that are incorporated in the trial data. For each
                // of the known scores we have to collect the actual measurements so we can start creating
                // a reference for scoring.
                //

                for (int i = 0; i < trialData.length(); i++) {
                    try {
                        JSONObject variety = trialData.getJSONObject(i);
                        List<String> data = new ArrayList<String>();
                        data.add(String.valueOf(variety.getInt("year")));
                        data.add(String.valueOf(variety.getString("date")));
                        data.add(String.valueOf(variety.getInt("trial")));
                        data.add(String.valueOf(variety.getInt("id")));

                        JSONObject traits = variety.getJSONObject("traits");
                        Iterator<String> keys = traits.keys();

                        while (keys.hasNext()) {
                            String key = keys.next();
                            JSONObject traitScore;

                            if (!traitScores.has(key)) {
                                traitScore = new JSONObject();
                                traitScores.put(key, traitScore);
                            } else {
                                traitScore = traitScores.getJSONObject(key);
                            }

                            JSONObject trait = traits.getJSONObject(key);

                            if (trait.has("value")) {
                                Integer score = trait.getInt("score");


                                Double value = trait.getDouble("value");
                                JSONArray scoreValues = new JSONArray();

                                if (traitScore.has(score.toString())) {
                                    JSONArray current = traitScore.getJSONArray(score.toString());
                                    for (int j = 0; j < current.length(); j++) {
                                        scoreValues.put(current.getDouble(j));
                                    }
                                } else {
                                    scoreValues.put(value);
                                    traitScore.put(score.toString(), scoreValues);
                                }

                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //
                // Extract a list of keys and sort these. Afterwards sort the traits as well and build
                // a sorted JSONObject for everything.
                //

                List<Integer> sortedKeys = new ArrayList<Integer>();
                Iterator<String> keys = traitScores.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    sortedKeys.add(Integer.parseInt(key));
                }

                Collections.sort(sortedKeys);
                JSONObject sortedTraits = new JSONObject();

                for (int i = 0; i < sortedKeys.size(); i++) {
                    String key = sortedKeys.get(i).toString();
                    // Add sorted keys to columns
                    columns.add(sortedKeys.get(i).toString());

                    try {
                        JSONObject trait = traitScores.getJSONObject(key);
                        JSONObject sortedTrait = new JSONObject();

                        List<Integer> sortedScores = new ArrayList<Integer>();
                        Iterator<String> scores = trait.keys();
                        while (scores.hasNext()) {
                            String score = scores.next();
                            sortedScores.add(Integer.parseInt(score));
                        }

                        Collections.sort(sortedScores);

                        for (int j = 0; j < sortedScores.size(); j++) {
                            String score = sortedScores.get(j).toString();

                            JSONArray scoreValues = trait.getJSONArray(score);

                            Double sum = 0.0;
                            int length = scoreValues.length();

                            for (int k = 0; k < scoreValues.length(); k++) {

                                Double value = scoreValues.getDouble(k);
                                sum += value;
                            }
                            double average = sum / length;

                            sortedTrait.put(score, average);
                        }

                        sortedTraits.put(key, sortedTrait);

                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                }

                //
                // Now that we have a fully sorted list of traits and have been added to the columns
                // write the first line of the CSV.
                //

                try {
                    String line = String.join(",", columns) + "\n";
                    FileUtils.writeStringToFile(csvFile, line);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                //
                // Iterate over the trial data again and now score any 0 values using the reference
                // scoring list that we have constructed earlier.
                //

                for (int i = 0; i < trialData.length(); i++) {
                    try {
                        JSONObject variety = trialData.getJSONObject(i);
                        List<String> data = new ArrayList<String>();
                        data.add(variety.getString("year"));
                        data.add(variety.getString("date"));
                        data.add(variety.getString("trial"));
                        data.add(variety.getString("id"));

                        JSONObject traits = variety.getJSONObject("traits");
                        Iterator<String> traitKeys = traits.keys();

                        while (traitKeys.hasNext()) {
                            String key = traitKeys.next();

                            JSONObject trait = traits.getJSONObject(key);
                            Integer score = trait.getInt("score");

                            if (score == 0 && trait.has("value")) {
                                Double value = trait.getDouble("value");
                                JSONObject traitDefaults = sortedTraits.getJSONObject(key);

                                List<Double> x = new ArrayList<Double>();
                                List<Integer> y = new ArrayList<Integer>();

                                Iterator<String> defaultKeys = traitDefaults.keys();
                                while (defaultKeys.hasNext()) {
                                    String defaultKey = defaultKeys.next();
                                    y.add(Integer.parseInt(defaultKey));
                                    x.add(traitDefaults.getDouble(defaultKey));
                                }

                                LinearInterpolator li = new LinearInterpolator(); // or other interpolator
                                double[] dblX = x.stream().mapToDouble(d -> d).toArray();
                                double[] dblY = y.stream().mapToDouble(d -> d).toArray();

//                                PolynomialSplineFunction psf = li.interpolate(dblX, dblY);
//                                double yi = psf.value(value);
//                                score = (int)yi;
                            }

                            data.add(String.valueOf(score));
                        }

                        try {
                            String line = String.join(",", data) + "\n";
                            FileUtils.writeStringToFile(csvFile, line, true);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                }

//                Intent intentShareFile = new Intent(Intent.ACTION_SEND);
//
//                if (csvFile.exists()) {
////                    intentShareFile.setType("text/csv");
//
//                    intentShareFile.setType(URLConnection.guessContentTypeFromName(csvFile.getName()));
//                    intentShareFile.setClipData(ClipData.newRawUri("", Uri.parse("file://" + csvFile.getAbsolutePath())));
//                    intentShareFile.addFlags(
//                            Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//
//                    intentShareFile.putExtra(Intent.EXTRA_STREAM,
//                            Uri.parse("file://" + csvFile.getAbsolutePath()));
//
////                    File imagePath = new File(Context.getFilesDir(), "my_images");
////                    File newFile = new File(imagePath, "default_image.jpg");
////                    Uri contentUri = getUriForFile(getContext(), "com.mydomain.fileprovider", newFile);
//
////                    intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + csvFile.getAbsolutePath()));
//
////                    intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
////                            "Sharing File...");
////                    intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing File...");
//
//                    startActivity(Intent.createChooser(intentShareFile, "Share File"));
//                }
            }
        });

        // Debug mode
        buttonTest.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SuspiciousIndentation")
            public void onClick(View v) {
                try {
                    debugProcessor(imageView, user_cfg);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void debugProcessor(ImageView imageView, Config user_cfg) throws IOException, JSONException {
        // Read image
        Mat img = open_image_file(mTestImages[index]);

        // For testing only
        DepthData depth_data = new DepthData();
        depth_data.distance = 600;

        // Process the image
        List<Instance> all_objects = Processor.full_process_image(mTestImages[index], img, user_cfg, depth_data, fov, getApplicationContext());

        // Draw instances
        Mat img_output_final = ImageOps.draw_instances_tomato(img, all_objects, user_cfg);

        // Display output
        ImageOps.displayImage(img_output_final, imageView);

        if (index != mTestImages.length-1){
            index = index + 1;
        }
        else{
            index = 0;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK) {
                        String metadata = data.getStringExtra("metadata");
                        float distance = data.getFloatExtra("distance", 0f);

                        try {
                            JSONObject imageData = new JSONObject(metadata);
                            imageData.put("distance", distance);
                            String date = imageData.getString("date");
                            String label = imageData.getString("label");
                            int id = imageData.getInt("id");


                            String outputImgPath = Trials.getTrialOutputImg(imageData);

                            int idx = tomatoList.size();
                            this.tomatoList.add(new Tomato(label, id, date, outputImgPath));
                            this.adapter.notifyItemInserted(idx);

                            JSONArray trialData = Trials.loadTrialData(getApplicationContext());
                            trialData.put(imageData);

                            Trials.saveTrialData(getApplicationContext(), trialData);

                            Log.i("Trial Data", String.valueOf(trialData));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
            }
        }
    }

}
