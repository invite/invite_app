/*
 * Copyright 2020 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wur.invite.traitdetector.depth;

import android.content.Context;
import android.content.Intent;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.media.Image;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.util.SizeF;
import android.view.View;
import android.widget.Toast;

import com.google.ar.core.ArCoreApk;
import com.google.ar.core.Camera;
import com.google.ar.core.CameraConfig;
import com.google.ar.core.CameraConfigFilter;
import com.google.ar.core.Config;
import com.google.ar.core.Frame;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;
import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.core.exceptions.UnavailableApkTooOldException;
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException;
import com.google.ar.core.exceptions.UnavailableDeviceNotCompatibleException;
import com.google.ar.core.exceptions.UnavailableSdkTooOldException;
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException;
import com.wur.invite.traitdetector.R;
import com.wur.invite.traitdetector.common.helpers.CameraPermissionHelper;
import com.wur.invite.traitdetector.common.helpers.DisplayRotationHelper;
import com.wur.invite.traitdetector.common.helpers.FullScreenHelper;
import com.wur.invite.traitdetector.common.helpers.SnackbarHelper;
import com.wur.invite.traitdetector.common.helpers.TrackingStateHelper;
import com.wur.invite.traitdetector.common.rendering.BackgroundRenderer;
import com.wur.invite.traitdetector.qr.QrCodeHandler;

import org.json.JSONObject;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class ARActivity extends AppCompatActivity implements GLSurfaceView.Renderer {
    private static final String TAG = ARActivity.class.getSimpleName();
    private boolean installRequested;

    // Rendering. The Renderers are created here, and initialized when the GL surface is created.
    private GLSurfaceView surfaceView;

    private Session session;
    private final SnackbarHelper messageSnackbarHelper = new SnackbarHelper();
    private DisplayRotationHelper displayRotationHelper;
    private final TrackingStateHelper trackingStateHelper = new TrackingStateHelper(this);

    private final BackgroundRenderer backgroundRenderer = new BackgroundRenderer();
    private final DepthHandler depthTexture = new DepthHandler();

    // Temporary matrix allocated here to reduce number of allocations for each frame.
    private static final String DEPTH_NOT_AVAILABLE_MESSAGE = "[Depth not supported on this device]";
    private boolean messageChanged = false;
    private String messageToShow = "";
    private boolean capturePicture = false;

    private QrCodeHandler qrCodeHandler = new QrCodeHandler();
    private boolean qrCodeRetrieved = false;
    private boolean qrEnabled = false;
    public JSONObject qrCodeValues = new JSONObject();
//    public float[] cameraDistance = new float[] {0, 0};
    public float cameraDistance = 0;

    private boolean isDepthSupported = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ar_activity);
        surfaceView = findViewById(R.id.surfaceview);
        displayRotationHelper = new DisplayRotationHelper(/*context=*/ this);

        // Set up renderer.
        surfaceView.setPreserveEGLContextOnPause(true);
        surfaceView.setEGLContextClientVersion(2);
        surfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0); // Alpha used for plane blending.
        surfaceView.setRenderer(this);
        surfaceView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
        surfaceView.setWillNotDraw(false);

        installRequested = false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (session == null) {
            Exception exception = null;
            String message = null;
            try {
                switch (ArCoreApk.getInstance().requestInstall(this, !installRequested)) {
                    case INSTALL_REQUESTED:
                        installRequested = true;
                        return;
                    case INSTALLED:
                        break;
                }

                // ARCore requires camera permissions to operate. If we did not yet obtain runtime
                // permission on Android M and above, now is a good time to ask the user for it.
                if (!CameraPermissionHelper.hasCameraPermission(this)) {
                    CameraPermissionHelper.requestCameraPermission(this);
                    return;
                }

                // Creates the ARCore session.
                session = new Session(/* context= */ this);
                Config config = session.getConfig();


                isDepthSupported = session.isDepthModeSupported(Config.DepthMode.AUTOMATIC);
                if (isDepthSupported) {
                    config.setDepthMode(Config.DepthMode.AUTOMATIC);
                } else {
                    config.setDepthMode(Config.DepthMode.DISABLED);
                }

                CameraConfigFilter filter = new CameraConfigFilter(session);
                List<CameraConfig> hdConfigs = session.getSupportedCameraConfigs(filter).stream()
//                        .filter(cameraConfig -> cameraConfig.getImageSize().getHeight() == 720 && cameraConfig.getTextureSize().getHeight() == 1080)
                        .filter(cameraConfig -> cameraConfig.getImageSize().getHeight() == 1080 && cameraConfig.getTextureSize().getHeight() == 1080)
//                        .map(cameraConfig -> {
//                            System.out.println(cameraConfig.getImageSize());
//                            System.out.println(cameraConfig.getTextureSize());
//                            return cameraConfig;
//                        })
                        .collect(Collectors.toList());
                session.setCameraConfig(hdConfigs.get(0));
                config.setFocusMode(Config.FocusMode.AUTO);
                session.configure(config);

            } catch (UnavailableArcoreNotInstalledException
                    | UnavailableUserDeclinedInstallationException e) {
                message = "Please install ARCore";
                exception = e;
            } catch (UnavailableApkTooOldException e) {
                message = "Please update ARCore";
                exception = e;
            } catch (UnavailableSdkTooOldException e) {
                message = "Please update this app";
                exception = e;
            } catch (UnavailableDeviceNotCompatibleException e) {
                message = "This device does not support AR";
                exception = e;
            } catch (Exception e) {
                message = "Failed to create AR session";
                exception = e;
            }

            if (message != null) {
                messageSnackbarHelper.showError(this, message);
                Log.e(TAG, "Exception creating session", exception);
                return;
            }
        }

        // Note that order matters - see the note in onPause(), the reverse applies here.
        try {
            session.resume();
            session.pause();
            session.resume();
        } catch (CameraNotAvailableException e) {
            messageSnackbarHelper.showError(this, "Camera not available. Try restarting the app.");
            session = null;
            return;
        }

        surfaceView.onResume();
        displayRotationHelper.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (session != null) {
            // Note that the order matters - GLSurfaceView is paused first so that it does not try
            // to query the session. If Session is paused before GLSurfaceView, GLSurfaceView may
            // still call session.update() and get a SessionPausedException.
            displayRotationHelper.onPause();
            surfaceView.onPause();
            session.pause();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] results) {
        if (!CameraPermissionHelper.hasCameraPermission(this)) {
            Toast.makeText(this, "Camera permission is needed to run this application",
                    Toast.LENGTH_LONG).show();
            if (!CameraPermissionHelper.shouldShowRequestPermissionRationale(this)) {
                // Permission denied with checking "Do not ask again".
                CameraPermissionHelper.launchPermissionSettings(this);
            }
            finish();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        FullScreenHelper.setFullScreenOnWindowFocusChanged(this, hasFocus);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        GLES20.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

        // Prepare the rendering objects. This involves reading shaders, so may throw an IOException.
        try {
            depthTexture.createOnGlThread();
            // Create the texture and pass it to ARCore session to be filled during update().
            backgroundRenderer.createOnGlThread(/*context=*/ this);
            backgroundRenderer.createDepthShaders(/*context=*/ this, depthTexture.getDepthTexture());
        } catch (IOException e) {
            Log.e(TAG, "Failed to read an asset file", e);
        }
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        displayRotationHelper.onSurfaceChanged(width, height);
        GLES20.glViewport(0, 0, width, height);
    }


    public String checkDistance(float distance){
        String message;
        if (distance == 0) {
            message = "Distance: Initialising";
        } else if (distance < 500) {
            message = "Distance: " + distance / 10 + " cm. Too close for accurate analysis";
        } else if (distance > 1200) {
            message = "Distance: " + distance / 10 + " cm. Too far for accurate analysis" ;
        } else {
            message = "Distance: " + distance / 10 + " cm";
        }
        return message;
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        // Clear screen to notify driver it should not load any pixels from previous frame.
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        if (session == null) {
            return;
        }
        // Notify ARCore session that the view size changed so that the perspective matrix and
        // the video background can be properly adjusted.
        displayRotationHelper.updateSessionIfNeeded(session);

        try {


                session.setCameraTextureName(backgroundRenderer.getTextureId());

                // Obtain the current frame from ARSession. When the configuration is set to
                // UpdateMode.BLOCKING (it is by default), this will throttle the rendering to the
                // camera framerate.
                Frame frame = session.update();
                Camera camera = frame.getCamera();

                // If frame is ready, render camera preview image to the GL surface.
                backgroundRenderer.draw(frame);

                // Keep the screen unlocked while tracking, but allow it to lock when tracking stops.
                trackingStateHelper.updateKeepScreenOnFlag(camera.getTrackingState());

                // If not tracking, don't draw 3D objects, show tracking failure reason instead.
//                if (camera.getTrackingState() == TrackingState.PAUSED) {
//                    messageSnackbarHelper.showMessage(
//                            this, TrackingStateHelper.getTrackingFailureReasonString(camera));
//                    return;
//                }

                float[] projmtx = new float[16];
                camera.getProjectionMatrix(projmtx, 0, 0.1f, 100.0f);
                // Get camera matrix and draw.
                float[] viewmtx = new float[16];
                camera.getViewMatrix(viewmtx, 0);
                final float[] colorCorrectionRgba = new float[4];
                frame.getLightEstimate().getColorCorrection(colorCorrectionRgba, 0);








                // No tracking error at this point. Inform user of what to do based on if planes are found.
                String message = "";

                Log.d("INVITE_X", String.valueOf(isDepthSupported) + " " + camera.getTrackingState());

                if (!isDepthSupported) {
                    message = DEPTH_NOT_AVAILABLE_MESSAGE;
                    messageSnackbarHelper.showMessage(this, message);
                }
                else {

                    Bundle extras = getIntent().getExtras();
                    assert extras != null;
                    qrEnabled = extras.getBoolean("qr_enabled");


                    // Image depthImage = frame.acquireDepthImage();
                    Image depthImage = frame.acquireDepthImage16Bits();
                    Image depthConfidence = frame.acquireRawDepthConfidenceImage();
                    Image cameraImage = frame.acquireCameraImage();

                    Image.Plane depthPlane = depthImage.getPlanes()[0];

                    int pixelStride = depthPlane.getPixelStride();
                    int rowStride = depthPlane.getRowStride();
                    ByteBuffer depthBufferRaw = depthPlane.getBuffer().order(ByteOrder.nativeOrder());

                    // Get camera distance
                    int[] depthShape = {depthImage.getWidth(), depthImage.getHeight()};
                    int depthX = depthImage.getWidth() / 2;
                    int depthY = depthImage.getHeight() / 2;
                    int depthIndex = depthX * depthPlane.getPixelStride() + depthY * depthPlane.getRowStride();
                    cameraDistance = depthBufferRaw.getShort(depthIndex);
//            cameraDistance = depthTexture.getDepthValue(depthImage, depthConfidence, depthX, depthY);
//            cameraDistance = depthTexture.getDepthOnly(depthImage, depthX, depthY);

                    float distance = cameraDistance;
//            float confidence = cameraDistance[1];

                    //
                    // Distance handler
                    //
                    message = checkDistance(distance);

                    // FORCE QR
                    //
                    // Show status message in camera interface
                    //
                    if (qrEnabled) {
                        if (!qrCodeRetrieved) {
                            message += "\nNo QR code found. Please try moving the camera slightly.";

                            qrCodeValues = qrCodeHandler.analyze(cameraImage);

                            if (qrCodeValues.length() != 0) {
                                qrCodeRetrieved = true;
                            }
                        } else {

//                if (!message.isEmpty()) {
//                    message += "\nDistance: " + distance/10 + "cm Variety: " + qrCodeValues.getString("label");
//                } else {
//                    message = "Distance: " + distance/10 + "cm Variety: " + qrCodeValues.getString("label");
//                }
                        }
                    } else {
                        message += "\nQR scanning is disabled. Now using default values.";
                        qrCodeValues = qrCodeHandler.defaultValues();
                    }

                    if (message != messageToShow) {
                        messageToShow = message;
                        messageSnackbarHelper.showMessage(this, messageToShow);
                    }

                    if (capturePicture && distance != 0) {
                        capturePicture = false;

                        // Save depth image
                        session.pause();
                        JSONObject traits = depthTexture.saveDepth(this, frame, qrCodeValues, cameraImage, pixelStride, rowStride, depthBufferRaw, depthShape);
                        qrCodeValues.put("traits", traits);

                        Intent output = new Intent();

                        String label = (String) qrCodeValues.get("label");
                        String metadata = qrCodeValues.toString();
                        output.putExtra("label", label);
                        output.putExtra("metadata", metadata);
                        output.putExtra("distance", distance / 10);

                        setResult(this.RESULT_OK, output);
                        finish();
                    }

                    depthImage.close();
                    cameraImage.close();
                }



        } catch (Throwable t) {
            // Avoid crashing the application due to unhandled exceptions.
//            Log.e(TAG, "Exception on the OpenGL thread", t);
        }
    }

    public void onSavePicture(View view) {
        // Here just a set a flag so we can copy
        // the image from the onDrawFrame() method.
        // This is required for OpenGL so we are on the rendering thread.

        if (cameraDistance > 300 && cameraDistance < 1200) {
            this.capturePicture = true;
        }
    }

}
