package com.wur.invite.traitdetector.processing.models;

import android.content.Context;
import android.util.Log;

import com.wur.invite.traitdetector.common.FileFinder;
import com.wur.invite.traitdetector.config.Config;
import com.wur.invite.traitdetector.processing.ImageOps;
import com.wur.invite.traitdetector.processing.Instance;
import com.wur.invite.traitdetector.processing.MatrixOps;
import com.wur.invite.traitdetector.processing.ORT;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import ai.onnxruntime.OnnxTensor;
import ai.onnxruntime.OrtException;
import ai.onnxruntime.OrtSession;

public class YOLOv8_2 {
    static OnnxTensor inputTensor;
    public static long[] INPUT_SHAPE = {1, 3, 640, 640};
    public static int MASK_SIZE = 160;

    static float score_threshold = 0.2f;
    static float iou_threshold = 0.5f;
    static String[] classes_list;
    static ORT.ONNXModel model;
    static int yolo_input_size = 640;
    static float box_padding = 0;

    public static void initModel(Context context, Config user_cfg) {
        String model_filename = null;
        try {
            model_filename = FileFinder.assetFilePath(context, user_cfg.yolo_model_filename_2);
        } catch (IOException e) {
            e.printStackTrace();
        }

        model = new ORT.ONNXModel(model_filename);
        score_threshold = user_cfg.yolo_score_threshold_2;
        iou_threshold = user_cfg.yolo_iou_threshold_2;
        classes_list = user_cfg.yolo_classes_list_2;
        yolo_input_size = user_cfg.yolo_input_size_2;
        box_padding = user_cfg.yolo_box_padding_2;
        INPUT_SHAPE = new long[]{1, 3, yolo_input_size, yolo_input_size};
    }

    public static Map<String, OnnxTensor> preprocess(Mat img) throws OrtException {

        // Resizing and BGR -> RGB
        Mat resizedImg = new Mat();

        // ImageUtils.resizeWithPadding(img, resizedImg, INPUT_SIZE, INPUT_SIZE);   // option 1
        Imgproc.resize(img, resizedImg, new Size(yolo_input_size, yolo_input_size));          // option 2
        Imgproc.cvtColor(resizedImg, resizedImg, Imgproc.COLOR_BGR2RGB);

        // To OnnxTensor
        Map<String, OnnxTensor> container = new HashMap<>();
        resizedImg.convertTo(resizedImg, CvType.CV_32FC1, 1. / 255);
        float[] whc = new float[3 * yolo_input_size * yolo_input_size];
        resizedImg.get(0, 0, whc);
        float[] chw = ImageOps.whc2cwh(whc);
        FloatBuffer inputBuffer = FloatBuffer.wrap(chw);
        inputTensor = OnnxTensor.createTensor(model.env, inputBuffer, INPUT_SHAPE);
        container.put(model.input_name, inputTensor);

        return container;
    }

    public static List<Instance> process_image(Mat img) {

        // Preprocess image
        Map<String, OnnxTensor> input_container = null;
        try {
            input_container = preprocess(img);
        } catch (OrtException e) {
            e.printStackTrace();
        }


        // Get results
        float[][] predictions1 = new float[0][];
        float[][][] predictions2 = new float[0][][];
        OrtSession.Result results = null;
        try {
            results = model.session.run(input_container);
            predictions1 = ((float[][][]) results.get(0).getValue())[0];
            predictions2 = ((float[][][][]) results.get(1).getValue())[0];
        } catch (OrtException e) {
            e.printStackTrace();
        }


        // Postprocess results
//        List<Instance> instancesList = process_box_output(img, predictions1);
//        process_mask_output(img, instancesList, predictions2);
        List<Instance> instancesList = process_box_and_mask_output(img, predictions1, predictions2);

        return instancesList;
    }


    public static List<Instance> process_box_and_mask_output (Mat img, float[][] predictions, float[][][] predictions2) {
        int INPUT_W = (int) img.size().width;
        int INPUT_H = (int) img.size().height;
        MASK_SIZE = predictions2[0].length;

        // predictions
        Map<Integer, List<float[]>> class2Bbox = new HashMap<>();
        float[][] preds = MatrixOps.matrix_transpose(predictions);
        for (float[] instance : preds) {
            // Filter by confidence threshold
            // Get output label
            float[] conditionalProbabilities = Arrays.copyOfRange(instance, 4, 4 + classes_list.length);
            int label = ImageOps.argmax(conditionalProbabilities);
            float s = conditionalProbabilities[label];
            if (s < score_threshold) continue;
            instance[4] = s;

            // xywh to (x1, y1, x2, y2)
            ImageOps.xywh2xyxy(instance);

            // skip invalid predictions
            if (instance[0] >= instance[2] || instance[1] >= instance[3]) continue;

            // xmin, ymin, xmax, ymax -> (xmin_org, ymin_org, xmax_org, ymax_org)
            ImageOps.rescale_boxes(instance, yolo_input_size, yolo_input_size, img.width(), img.height());

            class2Bbox.putIfAbsent(label, new ArrayList<>());
            Objects.requireNonNull(class2Bbox.get(label)).add(instance);
        }


        // Apply Non-max suppression for each class
        List<Instance> outputs = new ArrayList<>();
        for (Map.Entry<Integer, List<float[]>> entry : class2Bbox.entrySet()) {
            int key = entry.getKey();
            List<float[]> instances = entry.getValue();
            int c;
            c = key;

            // Perform NMS
            instances = ImageOps.nms(instances, iou_threshold);
            for (float[] inst : instances) {
                float[] b = Arrays.copyOfRange(inst, 0, 4);
                b[0] = ImageOps.getMax(new int[]{(int) (b[0] * box_padding), 0});
                b[1] = ImageOps.getMax(new int[]{(int) (b[1] * box_padding), 0});
                b[2] = ImageOps.getMin(new int[]{(int) (b[2] * (1 + (1 - box_padding))), INPUT_W});
                b[3] = ImageOps.getMin(new int[]{(int) (b[3] * (1 + (1 - box_padding))), INPUT_H});

                float s = inst[4];
                int l = inst.length;
                float[] m = Arrays.copyOfRange(inst, 4 + classes_list.length, l);
                Mat m_mat = new Mat(1, m.length, CvType.CV_32F);
                m_mat.put(0, 0, m);


                Instance instance = new Instance(c, b, s, m);
                outputs.add(instance);




                // Make a mask matrix
                float[][] mask_matrix = new float[32][MASK_SIZE*MASK_SIZE];
//                Mat mask_mat = new Mat(32, MASK_SIZE*MASK_SIZE, CvType.CV_32F);
                int index = 0;
                for (float[][] mask_output : predictions2) {
                    float[] flattened_mask = MatrixOps.flatten(mask_output);
                    mask_matrix[index] = flattened_mask;
//                    mask_mat.put(index, 0, flattened_mask);
                    index = index + 1;
                }

                // Matrix multiplication of output[1] and mask outputs
                // Also apply sigmoid
                float[][] output_matrix = MatrixOps.matrix_multiplication(1, 32, new float[][]{m}, 32, MASK_SIZE*MASK_SIZE, mask_matrix);
                index = 0;
                for (float val : output_matrix[0]) {
                    val = MatrixOps.sigmoid(val);
                    output_matrix[0][index] = val;
                    index = index + 1;
                }

//                Mat output_matrix_mat = new Mat();
//                Core.multiply(mask_mat, m_mat, output_matrix_mat);





                // Reshape the mask into a mat
                Mat mask_output = new Mat(MASK_SIZE, MASK_SIZE, CvType.CV_32F);
                mask_output.put(0, 0, output_matrix[0]);

                // Scale the boxes according to mask size
                float[] scale_box = b.clone();
                ImageOps.rescale_boxes(scale_box, INPUT_W, INPUT_H, MASK_SIZE, MASK_SIZE);

                // Prepare a mask map with 0s
                Mat mask_map = new Mat(INPUT_H, INPUT_W, CvType.CV_8U, Scalar.all(0));

                int blursize1 = (int) INPUT_W / MASK_SIZE;
                int blursize2 = (int) INPUT_H / MASK_SIZE;
                int scale_x1 = (int) scale_box[0];
                int scale_y1 = (int) scale_box[1];
                int scale_x2 = (int) scale_box[2];
                int scale_y2 = (int) scale_box[3];
                int x1 = (int) b[0];
                int y1 = (int) b[1];
                int x2 = (int) b[2];
                int y2 = (int) b[3];

                // Get the mask from a region of the mask output
                Rect region = new Rect(scale_x1, scale_y1, scale_x2 - scale_x1, scale_y2 - scale_y1);
                Mat scale_crop_mask = new Mat(mask_output, region);

                // Resize the mask according to the actual image size
                Mat crop_mask = new Mat();
                Imgproc.resize(scale_crop_mask, crop_mask, new Size(x2 - x1, y2 - y1));

                // Blur the cropped mask
                Imgproc.blur(crop_mask, crop_mask, new Size(blursize1, blursize2));

                // Get pixels that are above 0.5
                Imgproc.threshold(crop_mask, crop_mask, 0.5, 1.0, Imgproc.THRESH_BINARY);

                // Convert to int8
                crop_mask.convertTo(crop_mask, CvType.CV_8U, 255);

                // Put the submat to the mask map
                Mat dest_mat = mask_map.submat(y1, y2, x1, x2);
                crop_mask.copyTo(dest_mat);

                instance.m_map = mask_map;

            }
        }
        return outputs;
    }

    public static List<Instance> process_box_output(Mat img, float[][] predictions) {
        int INPUT_W = (int) img.size().width;
        int INPUT_H = (int) img.size().height;

        // predictions
        Map<Integer, List<float[]>> class2Bbox = new HashMap<>();
        float[][] preds = MatrixOps.matrix_transpose(predictions);
        for (float[] instance : preds) {
            // Filter by confidence threshold
            // Get output label
            float[] conditionalProbabilities = Arrays.copyOfRange(instance, 4, 4 + classes_list.length);
            int label = ImageOps.argmax(conditionalProbabilities);
            float s = conditionalProbabilities[label];
            if (s < score_threshold) continue;
            instance[4] = s;

            // xywh to (x1, y1, x2, y2)
            ImageOps.xywh2xyxy(instance);

            // skip invalid predictions
            if (instance[0] >= instance[2] || instance[1] >= instance[3]) continue;

            // xmin, ymin, xmax, ymax -> (xmin_org, ymin_org, xmax_org, ymax_org)
            ImageOps.rescale_boxes(instance, yolo_input_size, yolo_input_size, img.width(), img.height());

            class2Bbox.putIfAbsent(label, new ArrayList<>());
            Objects.requireNonNull(class2Bbox.get(label)).add(instance);
        }


        // Apply Non-max suppression for each class
        List<Instance> outputs = new ArrayList<>();
        for (Map.Entry<Integer, List<float[]>> entry : class2Bbox.entrySet()) {
            int key = entry.getKey();
            List<float[]> instances = entry.getValue();
            int c;
            c = key;

            // Perform NMS
            instances = ImageOps.nms(instances, iou_threshold);
            for (float[] instance : instances) {
                float[] b = Arrays.copyOfRange(instance, 0, 4);
                b[0] = ImageOps.getMax(new int[]{(int) (b[0] * box_padding), 0});
                b[1] = ImageOps.getMax(new int[]{(int) (b[1] * box_padding), 0});
                b[2] = ImageOps.getMin(new int[]{(int) (b[2] * (1 + (1 - box_padding))), INPUT_W});
                b[3] = ImageOps.getMin(new int[]{(int) (b[3] * (1 + (1 - box_padding))), INPUT_H});

                float s = instance[4];
                int l = instance.length;
                float[] m = Arrays.copyOfRange(instance, 4 + classes_list.length, l);
                outputs.add(new Instance(c, b, s, m));

            }
        }

        return outputs;
    }

    public static void process_mask_output(Mat img, List<Instance> instancesList, float[][][] predictions2) {
        int INPUT_W = (int) img.size().width;
        int INPUT_H = (int) img.size().height;
        MASK_SIZE = predictions2[0].length;

        for (Instance instance : instancesList) {
            float[] mask = instance.m;
            float[] box = instance.b;

            // Make a mask matrix
            float[][] mask_matrix = new float[32][MASK_SIZE*MASK_SIZE];
            int index = 0;
            for (float[][] mask_output : predictions2) {
                float[] flattened_mask = MatrixOps.flatten(mask_output);
                mask_matrix[index] = flattened_mask;
                index = index + 1;
            }

            // Matrix multiplication of output[1] and mask outputs
            // Also apply sigmoid
            float[][] output_matrix = MatrixOps.matrix_multiplication(1, 32, new float[][]{mask}, 32, MASK_SIZE*MASK_SIZE, mask_matrix);
            index = 0;
            for (float val : output_matrix[0]) {
                val = MatrixOps.sigmoid(val);
                output_matrix[0][index] = val;
                index = index + 1;
            }

            // Reshape the mask into a mat
            Mat mask_output = new Mat(MASK_SIZE, MASK_SIZE, CvType.CV_32F);
            mask_output.put(0, 0, output_matrix[0]);

            // Scale the boxes according to mask size
            float[] scale_box = box.clone();
            ImageOps.rescale_boxes(scale_box, INPUT_W, INPUT_H, MASK_SIZE, MASK_SIZE);

            // Prepare a mask map with 0s
            Mat mask_map = new Mat(INPUT_H, INPUT_W, CvType.CV_8U, Scalar.all(0));

            int blursize1 = (int) INPUT_W / MASK_SIZE;
            int blursize2 = (int) INPUT_H / MASK_SIZE;
            int scale_x1 = (int) scale_box[0];
            int scale_y1 = (int) scale_box[1];
            int scale_x2 = (int) scale_box[2];
            int scale_y2 = (int) scale_box[3];
            int x1 = (int) instance.b[0];
            int y1 = (int) instance.b[1];
            int x2 = (int) instance.b[2];
            int y2 = (int) instance.b[3];

            // Get the mask from a region of the mask output
            Rect region = new Rect(scale_x1, scale_y1, scale_x2 - scale_x1, scale_y2 - scale_y1);
            Mat scale_crop_mask = new Mat(mask_output, region);

            // Resize the mask according to the actual image size
            Mat crop_mask = new Mat();
            Imgproc.resize(scale_crop_mask, crop_mask, new Size(x2 - x1, y2 - y1));

            // Blur the cropped mask
            Imgproc.blur(crop_mask, crop_mask, new Size(blursize1, blursize2));

            // Get pixels that are above 0.5
            Imgproc.threshold(crop_mask, crop_mask, 0.5, 1.0, Imgproc.THRESH_BINARY);

            // Convert to int8
            crop_mask.convertTo(crop_mask, CvType.CV_8U, 255);

            // Put the submat to the mask map
            Mat dest_mat = mask_map.submat(y1, y2, x1, x2);
            crop_mask.copyTo(dest_mat);

            instance.m_map = mask_map;
        }
    }


}
