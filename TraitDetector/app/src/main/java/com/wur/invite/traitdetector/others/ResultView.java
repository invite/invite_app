// Copyright (c) 2020 Facebook, Inc. and its affiliates.
// All rights reserved.
//
// This source code is licensed under the BSD-style license found in the
// LICENSE file in the root directory of this source tree.

package com.wur.invite.traitdetector.others;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;


public class ResultView extends View {

    private final static int TEXT_X = 0;
    private final static int TEXT_Y = 0;
    private final static int TEXT_WIDTH = 130;
    private final static int TEXT_HEIGHT = 25;

    private Paint mPaintRectangle;
    private Paint mPaintText;
    private ArrayList<Result> mResults;
    private Bitmap mResizedBitmap;
    private int mRaw_w;
    private int mRaw_h;


    public ResultView(Context context) {
        super(context);
    }

    public ResultView(Context context, AttributeSet attrs){
        super(context, attrs);
        mPaintRectangle = new Paint();
        mPaintRectangle.setColor(Color.YELLOW);
        mPaintText = new Paint();
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mResizedBitmap != null) {
            Bitmap bmpSegmentation = Bitmap.createScaledBitmap(mResizedBitmap, mRaw_w, mRaw_h, true);
            Bitmap outputBitmap = bmpSegmentation.copy(bmpSegmentation.getConfig(), true);
            Mat blankMat = new Mat(bmpSegmentation.getHeight(), bmpSegmentation.getWidth(), CvType.CV_8UC3, new Scalar(0));


            // Must be 8UC3!!
            Mat outputMat = new Mat(bmpSegmentation.getHeight(), bmpSegmentation.getWidth(), CvType.CV_8UC3);
            Utils.bitmapToMat(outputBitmap, outputMat);


            if (mResults == null) return;

            for (Result result : mResults) {

                // Get output from results
//                Rect rectangle = result.rect;
//                float[] output_mask = result.output_mask;
//                int start_point_x = rectangle.left;
//                int start_point_y = rectangle.top;
//                int end_point_x = rectangle.right;
//                int end_point_y = rectangle.bottom;
//                int box_h = Math.abs(start_point_y - end_point_y);
//                int box_w = Math.abs(start_point_x - end_point_x);
//
//
//                // Show segmentation results
//                Mat mask_mat = new Mat(28, 28, CvType.CV_32F, new Scalar(0));
//                Mat image_res1 = new Mat(box_w, box_h, CvType.CV_8U);
//                Mat image_res2 = new Mat(box_w, box_h, CvType.CV_8U);
//                Mat image_res3 = new Mat(box_w, box_h, CvType.CV_8U);
//
//
//                mask_mat.put(0, 0, output_mask);
//
//                mask_mat.convertTo(mask_mat, CvType.CV_8U);
//
//
//                // Resize Image and puts result to image_res
//                Imgproc.resize(mask_mat,
//                        image_res1,
//                        new Size(box_w, box_h),
//                        0,
//                        0,
//                        Imgproc.INTER_AREA);
//                Imgproc.resize(mask_mat,
//                        image_res2,
//                        new Size(box_w, box_h),
//                        0,
//                        0,
//                        Imgproc.INTER_AREA);
//                Imgproc.resize(mask_mat,
//                        image_res3,
//                        new Size(box_w, box_h),
//                        0,
//                        0,
//                        Imgproc.INTER_AREA);
//                List<Mat> listMat = Arrays.asList(image_res1, image_res2, image_res3);
//                Mat dst = new Mat();
//                Core.merge(listMat, dst);
//
//                int end_point_yy = start_point_y + box_h;
//                int end_point_xx = start_point_x + box_w;
//
//
//                Mat bSubmat = blankMat.submat(start_point_y,
//                        end_point_yy,
//                        start_point_x,
//                        end_point_xx);
//                dst.copyTo(bSubmat);
//
//                // Draw rectangle
//                mPaintRectangle.setStrokeWidth(3);
//                mPaintRectangle.setStyle(Paint.Style.STROKE);
//                canvas.drawRect(result.rect, mPaintRectangle);
//                Paint mPaintRectangle = new Paint();
//                mPaintRectangle.setColor(Color.YELLOW);
//                mPaintRectangle.setStrokeWidth(3);
//                mPaintRectangle.setStyle(Paint.Style.STROKE);
//                canvas.drawRect(result.rect, mPaintRectangle);
//
//                // Show text
//                int TEXT_X = 0;
//                int TEXT_Y = 0;
//                int TEXT_WIDTH = 130;
//                int TEXT_HEIGHT = 25;
//                Paint mPaintText = new Paint();
//                Path mPath = new Path();
//                RectF mRectF = new RectF(result.rect.left, result.rect.top - TEXT_Y, result.rect.left + TEXT_WIDTH, result.rect.top + TEXT_Y);
//                mPath.addRect(mRectF, Path.Direction.CW);
//                mPaintText.setColor(Color.YELLOW);
//                canvas.drawPath(mPath, mPaintText);
//                mPaintText.setColor(Color.WHITE);
//                mPaintText.setStrokeWidth(0);
//                mPaintText.setStyle(Paint.Style.FILL);
//                mPaintText.setTextSize(20);
//                String text_string = String.format("%s %.2f | %d", MaskRCNN.mClasses[result.classIndex], result.score, box_w*box_h);
//                canvas.drawText(text_string, result.rect.left + TEXT_X, result.rect.top + TEXT_Y, mPaintText);
//
//
//                Imgproc.putText(outputMat,
//                        text_string,
//                        new Point(start_point_x, start_point_y-15),
//                        Imgproc.FONT_HERSHEY_SIMPLEX,
//                        0.5,
//                        new Scalar(255, 255, 255),
//                        1);

            }

            Imgproc.cvtColor(outputMat, outputMat, Imgproc.COLOR_BGRA2BGR);

            Mat binaryMat = new Mat();
            Core.bitwise_or(outputMat, blankMat, binaryMat);
//            Core.bitwise_and(outputMat, blankMat, binaryMat);
            Utils.matToBitmap(binaryMat, outputBitmap);
            outputBitmap = Bitmap.createScaledBitmap(outputBitmap,
                                            getWidth(),
                                            getHeight(),
                                            true);

//            Core.bitwise_and(outputMat, blankMat, outputMat);
//            Utils.matToBitmap(outputMat, outputBitmap);
            canvas.drawBitmap(outputBitmap, null, new Rect(0,
                                                    0,
                                                    getWidth(),
                                                    getHeight()), null);

            /*
            String filename = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES) + "/Invite" + "/Img_Output" +
                    Long.toHexString(System.currentTimeMillis()) + ".png";
            try (FileOutputStream out = new FileOutputStream(filename)) {
                outputBitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                // PNG is a lossless format, the compression factor (100) is ignored
            } catch (IOException e) {
                e.printStackTrace();
            }
             */
        }
    }

    public void setResults(Bitmap resizedBitmap,
                           int raw_w,
                           int raw_h,
                           ArrayList<Result> results) {
        mResults = results;
        mResizedBitmap = resizedBitmap;
        mRaw_h = raw_h;
        mRaw_w = raw_w;

    }
}
