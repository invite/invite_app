package com.wur.invite.traitdetector.processing;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;


public class Traits {

//        Trait map:  26 Size mm2
//                    27 Ratio L/D
//                    28 Longitudinal shape
//                    30 Peduncle depression
//                    31 Peduncle Scar Size
//                    33 Blossom End Shape
//                    34 Diameter
//                    37 Color
//                    101 Height
//                    102 Volume

    static String SIZE = "26";
    static String RATIO = "27";
    static String LONG_SHAPE = "28";
    static String PEDUNCLE_DEPRESSION = "30";
    static String PEDUNCLE_SIZE = "31";
    static String BLOSSOM_SHAPE = "33";
    static String DIAMETER = "34";
    static String HEIGHT = "101";
    static String VOLUME = "102";
    static String DISTANCE = "DISTANCE";

    List<Instance> instances;
    public Dictionary<String, Float> traits_data = new Hashtable<>();


    static String[] traitCodes = new String[]{SIZE, RATIO, LONG_SHAPE, PEDUNCLE_DEPRESSION, PEDUNCLE_SIZE, BLOSSOM_SHAPE, DIAMETER, HEIGHT, VOLUME};
    static String[] traitStrings = new String[]{"size", "shape_ratio", "longitudinal_shape", "peduncle_depression", "peduncle_size", "blossom_shape", "diameter", "height", "volume"};
    public Dictionary<String, String> traitDescriptions = new Hashtable<String, String>();

    public void measure_traits(List<Instance> all_objects){
        ArrayList<Float> diametersMm = new ArrayList<>();
        ArrayList<Float> heightsMm = new ArrayList<>();
        ArrayList<Float> volumesMm = new ArrayList<>();
        ArrayList<Float> ratiosMm = new ArrayList<>();
        ArrayList<Float> penduncleSizes = new ArrayList<>();

        ArrayList<Float> x1_vals = new ArrayList<>();
        ArrayList<Float> y1_vals = new ArrayList<>();
        ArrayList<Float> x2_vals = new ArrayList<>();
        ArrayList<Float> z_vals = new ArrayList<>();
        ArrayList<Float> x3_vals = new ArrayList<>();
        ArrayList<Float> y2_vals = new ArrayList<>();

        ArrayList<Float> y_vals = new ArrayList<>();
        ArrayList<Float> x_vals = new ArrayList<>();


        for (Instance o : all_objects) {
            if (o.orientation == 0) {
                x1_vals.add(o.widthMm);
                y1_vals.add(o.heightMm);
            }
            if (o.orientation == 1) {
                x2_vals.add(o.widthMm);
                z_vals.add(o.heightMm);
            }
            if (o.orientation == 2) {
                x3_vals.add(o.widthMm);
                y2_vals.add(o.heightMm);
            }
        }

        x_vals.addAll(x1_vals);
        x_vals.addAll(x2_vals);
        x_vals.addAll(x3_vals);
        y_vals.addAll(y1_vals);
        y_vals.addAll(y2_vals);

        float x_ave = (float) x_vals.stream().mapToDouble(a -> a).average().orElse(-1);
        float y_ave = (float) y_vals.stream().mapToDouble(a -> a).average().orElse(-1);
        float z_ave = (float) z_vals.stream().mapToDouble(a -> a).average().orElse(-1);
        float diameter_ave = (y_ave + z_ave) / 2;

        for (Instance o : all_objects) {
            float ratio = z_ave/o.widthMm;
            float volume = (float) ((4.0/3.0) * x_ave * y_ave * z_ave * Math.PI);
            if (o.orientation == 0) {
                float scar_size = (float) (o.widthMm_2 * o.heightMm_2 * Math.PI);
                penduncleSizes.add(scar_size);

            }
            else {
                penduncleSizes.add(0f);
            }
            diametersMm.add(diameter_ave);
            heightsMm.add(0f);
            volumesMm.add(volume);
            ratiosMm.add(ratio);

            int i = 0;
            for (String traitName: traitCodes){
                this.traitDescriptions.put(traitName, traitStrings[i]);
                i++;
            }
        }


        float ratio_ave = (float) ratiosMm.stream().mapToDouble(a -> a).average().orElse(-1);
        float volume_ave = (float) volumesMm.stream().mapToDouble(a -> a).average().orElse(-1);
        float scar_size_ave = (float) penduncleSizes.stream().mapToDouble(a -> a).average().orElse(-1);

        this.traits_data.put(SIZE, 0f);
        this.traits_data.put(RATIO, ratio_ave);
        this.traits_data.put(LONG_SHAPE, 0f);
        this.traits_data.put(PEDUNCLE_DEPRESSION, 0f);
        this.traits_data.put(BLOSSOM_SHAPE, 0f);
        this.traits_data.put(DIAMETER, diameter_ave);
        this.traits_data.put(HEIGHT, 0f);
        this.traits_data.put(VOLUME, volume_ave);
        this.traits_data.put(PEDUNCLE_SIZE, scar_size_ave);



    }





    public JSONObject write_trait_json()  throws JSONException {
        JSONObject traits_json = new JSONObject();
        for (String trait_ID : traitCodes) {

            float value = traits_data.get(trait_ID);
            Log.d("INVITE", trait_ID + " " + value);

            if (traits_json.has(trait_ID)) {
                JSONObject trait = traits_json.getJSONObject(trait_ID);
                trait.put("value", value);
            } else {
                JSONObject trait = new JSONObject();
                trait.put("score", 0);
                trait.put("value", value);
                trait.put("description", traitDescriptions.get(trait_ID));

                traits_json.put(trait_ID, trait);
            }
        }

        return traits_json;
    }


}




