package com.wur.invite.traitdetector.processing;

public class MatrixOps {

    public static byte[][] to2D(byte[] array, int m, int n) {
        byte[][] arr = new byte[m][n];
        for (int i = 0; i <arr.length ; i++) {
            for (int j = 0; j <arr[j].length ; j++) {
                arr[i][j] = array[i * arr[j].length + j];
            }
        }
        return arr;
    }

    public static float[][] reshape(float[][] A, int m, int n) {
        int origM = A.length;
        int origN = A[0].length;
        float[][] B = new float[m][n];
        float[] A1D = to1D(A);
        int index = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                B[i][j] = A1D[index++];
            }
        }
        return B;
    }




    public static float[] to1D(float[][] A) {
        float[] B = new float[A.length * A[0].length];
        int index = 0;
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[0].length; j++) {
                B[index++] = A[i][j];
            }
        }
        return B;
    }

    public static float[][] matrix_transpose(float[][] array) {
        // empty or unset array, nothing do to here
        if (array == null || array.length == 0)
            return array;

        int width = array.length;
        int height = array[0].length;

        float[][] array_new = new float[height][width];

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                array_new[y][x] = array[x][y];
            }
        }
        return array_new;
    }

    public static float[][] matrix_multiplication(int row1, int col1, float[][] A, int row2, int col2, float[][] B) {
        int i, j, k;
        if (row2 != col1) {
            System.out.println("\nMultiplication Not Possible");
        }

        float[][] C = new float[row1][col2];
        for (i = 0; i < row1; i++) {
            for (j = 0; j < col2; j++) {
                for (k = 0; k < row2; k++)
                    C[i][j] += A[i][k] * B[k][j];
            }
        }
        return C;
    }

    public static float[] flatten(float[][] mat) {
        float[] result = new float[mat.length * mat[0].length];
        for (int i = 0; i < mat.length; ++i) {
            System.arraycopy(mat[i], 0, result, i * mat[0].length, mat[i].length);
        }
        return result;
    }

    public static float sigmoid(float x) {
        return (float) (1 / (1 + Math.exp(-x)));
    }
}
