package com.wur.invite.traitdetector.gallery;

public class Cropped {

    private String imageURI;

    public Cropped(String imageURI) {
        this.imageURI = imageURI;
    }

    public String getImageURI() {
        return imageURI;
    }

    public void setImageURI(String imageURI) {
        this.imageURI = imageURI;
    }
}
