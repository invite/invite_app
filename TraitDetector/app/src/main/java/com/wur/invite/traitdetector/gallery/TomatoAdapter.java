package com.wur.invite.traitdetector.gallery;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wur.invite.traitdetector.R;

import java.util.ArrayList;

public class TomatoAdapter extends RecyclerView.Adapter<TomatoAdapter.TomatoViewHolder> {
    private ArrayList<Tomato> tomatoes;

    public TomatoAdapter(ArrayList<Tomato> tomatoes) {
        this.tomatoes = tomatoes;
    }

    @NonNull
    @Override
    public TomatoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_view, parent, false);

        return new TomatoViewHolder(v);
    }

    public void onBindViewHolder(@NonNull TomatoViewHolder holder, int position) {
        Tomato tomato = tomatoes.get(position);
        holder.name.setText(tomato.getName());
        holder.id.setText(String.valueOf(tomato.getId()));
        holder.date.setText(tomato.date);
        Drawable d = Drawable.createFromPath(tomato.getImageURI());
        holder.image.setImageDrawable(d);
    }

    @Override
    public int getItemCount() {
       if (tomatoes != null) {
           return tomatoes.size();
       } else {
           return 0;
       }
    }

    public static class TomatoViewHolder extends RecyclerView.ViewHolder {
        public final View view;
        public final TextView name;
        public final TextView id;
        public final TextView date;
        public final ImageView image;

        public TomatoViewHolder(View view) {
            super(view);

            this.view = view;
            name = view.findViewById(R.id.itemName);
            id = view.findViewById(R.id.itemId);
            date = view.findViewById(R.id.dateValue);
            image = view.findViewById(R.id.croppedImage);
        }
    }
}

