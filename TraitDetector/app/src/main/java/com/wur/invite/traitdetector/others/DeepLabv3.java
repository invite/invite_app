package com.wur.invite.traitdetector.others;

import android.content.Context;
import android.util.Log;

import com.wur.invite.traitdetector.common.FileFinder;
import com.wur.invite.traitdetector.config.Config;
import com.wur.invite.traitdetector.processing.ImageOps;
import com.wur.invite.traitdetector.processing.ORT;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ai.onnxruntime.OnnxTensor;
import ai.onnxruntime.OrtException;
import ai.onnxruntime.OrtSession;

public class DeepLabv3 {
    public static long[] INPUT_SHAPE = {3, 640, 640};
    static OnnxTensor inputTensor;
    static ORT.ONNXModel deeplab_model;
    static int deeplab_input_size = 512;
    static int deeplab_output_size = 512;

    public static void initModel(Context context, Config user_cfg){
        String model_filename = null;
        try {
            model_filename = FileFinder.assetFilePath(context, user_cfg.deeplab_model_filename);
        } catch (IOException e) {
            e.printStackTrace();
        }

        deeplab_model = new ORT.ONNXModel(model_filename);
        deeplab_input_size = user_cfg.deeplab_input_size;
        deeplab_output_size = user_cfg.deeplab_output_size;
        INPUT_SHAPE = new long[]{3, deeplab_input_size, deeplab_input_size};
    }


    public static Map<String, OnnxTensor> preprocess(Mat img) throws OrtException {

        // Resizing and BGR -> RGB
        Mat resizedImg = new Mat();

        Imgproc.resize(img, resizedImg, new Size(deeplab_input_size, deeplab_input_size));          // option 2
        Imgproc.cvtColor(resizedImg, resizedImg, Imgproc.COLOR_BGR2RGB);

        // To OnnxTensor
        Map<String, OnnxTensor> container = new HashMap<>();
        resizedImg.convertTo(resizedImg, CvType.CV_32FC1, 1. / 255);
        float[] whc = new float[3 * deeplab_input_size * deeplab_input_size];
        resizedImg.get(0, 0, whc);
        float[] chw = ImageOps.whc2cwh(whc);
        FloatBuffer inputBuffer = FloatBuffer.wrap(chw);

        inputTensor = OnnxTensor.createTensor(deeplab_model.env, inputBuffer, INPUT_SHAPE);
        container.put(deeplab_model.input_name, inputTensor);

        return container;
    }




    public static void process_image(Mat img) {

        // Preprocess image
        Map<String, OnnxTensor> input_container = null;
        try {
            input_container = preprocess(img);
        } catch (OrtException e) {
            e.printStackTrace();
        }


        // Get results
        float[][] ch1;
        float[][] ch2;
        float[][] ch3;
        OrtSession.Result results = null;
        Mat rgb_mat = null;
        try {
            results = deeplab_model.session.run(input_container);


            Log.d("hey", "ok");
        } catch (OrtException e) {
            e.printStackTrace();
        }


    }

}
