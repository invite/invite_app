package com.wur.invite.traitdetector.processing.models;

import android.content.Context;
import android.util.Log;

import com.wur.invite.traitdetector.common.FileFinder;
import com.wur.invite.traitdetector.config.Config;
import com.wur.invite.traitdetector.processing.ImageOps;
import com.wur.invite.traitdetector.processing.MatrixOps;
import com.wur.invite.traitdetector.processing.ORT;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ai.onnxruntime.OnnxTensor;
import ai.onnxruntime.OrtException;
import ai.onnxruntime.OrtSession;

public class SRGan {
    static OnnxTensor inputTensor;

    static ORT.ONNXModel sr_model;
    static int sr_input_size = 64;
    static int sr_output_size = 256;
    static int sr_force_size = 512;

    public static void initModel(Context context, Config user_cfg) {
        String model_filename = null;
        try {
            model_filename = FileFinder.assetFilePath(context, user_cfg.sr_model_filename);
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert model_filename != null;
        sr_model = new ORT.ONNXModel(model_filename);
        sr_input_size = user_cfg.sr_input_size;
        sr_output_size = user_cfg.sr_output_size;
        sr_force_size = user_cfg.sr_force_size;
    }


    public static Map<String, OnnxTensor> preprocess(Mat img) throws OrtException {

        // Resizing and BGR -> RGB
        Mat resizedImg = new Mat();

        // ImageUtils.resizeWithPadding(img, resizedImg, INPUT_SIZE, INPUT_SIZE);   // option 1
        Imgproc.resize(img, resizedImg, new Size(sr_input_size, sr_input_size));          // option 2
        Imgproc.cvtColor(resizedImg, resizedImg, Imgproc.COLOR_BGR2RGB);

        // To OnnxTensor
        Map<String, OnnxTensor> container = new HashMap<>();
        float[] whc = new float[3 * sr_input_size * sr_input_size];

        // Float32
        resizedImg.convertTo(resizedImg, CvType.CV_32FC1, 1. / 255);
        resizedImg.get(0, 0, whc);


//        resizedImg.convertTo(resizedImg, CvType.CV_16FC1, 1. / 255);
//        Size sizeA = resizedImg.size();
//        int index = 0;
//        for (int i = 0; i < sizeA.height; i++) {
//            for (int j = 0; j < sizeA.width; j++) {
//                double[] val = resizedImg.get(i, j);
//                for (int k = 0; k < val.length; k++) {
//                    whc[index] = (float) val[k];
//                    index = index + 1;
//                }
//            }
//        }


        // Put vals to buffer
        float[] chw = ImageOps.whc2cwh(whc);
        FloatBuffer inputBuffer = FloatBuffer.wrap(chw);



        long[] INPUT_SHAPE = {1, 3, 64, 64};
        inputTensor = OnnxTensor.createTensor(sr_model.env, inputBuffer, INPUT_SHAPE);
        container.put(sr_model.input_name, inputTensor);

        return container;
    }



    public static Mat process_image(Mat img) {

        // Preprocess image
        Map<String, OnnxTensor> input_container = null;
        try {
            input_container = preprocess(img);
        } catch (OrtException e) {
            e.printStackTrace();
        }


        // Get results
        float[][] ch1;
        float[][] ch2;
        float[][] ch3;
        OrtSession.Result results = null;
        Mat rgb_mat = null;
        try {
            results = sr_model.session.run(input_container);

            // Get per channel results
            ch1 = ((float[][][][]) results.get(0).getValue())[0][0];
            ch2 = ((float[][][][]) results.get(0).getValue())[0][1];
            ch3 = ((float[][][][]) results.get(0).getValue())[0][2];


            // Flatten the channels
            float[] flat_ch1 = MatrixOps.flatten(ch1);
            float[] flat_ch2 = MatrixOps.flatten(ch2);
            float[] flat_ch3 = MatrixOps.flatten(ch3);
            Mat ch1_mat = new Mat(sr_output_size, sr_output_size, CvType.CV_32F);
            ch1_mat.put(0, 0, flat_ch1);
            Mat ch2_mat = new Mat(sr_output_size, sr_output_size, CvType.CV_32F);
            ch2_mat.put(0, 0, flat_ch2);
            Mat ch3_mat = new Mat(sr_output_size, sr_output_size, CvType.CV_32F);
            ch3_mat.put(0, 0, flat_ch3);

            // Merge the mats
            List<Mat> mats = new ArrayList<>();
            mats.add(ch3_mat);
            mats.add(ch2_mat);
            mats.add(ch1_mat);
            rgb_mat = new Mat();
            Core.merge(mats, rgb_mat);

            // Normalize the mat (from 0-1 to 0-255)
            Core.normalize(rgb_mat, rgb_mat, 0.0, 255, Core.NORM_MINMAX, CvType.CV_8UC3);

            // Resize to force size
            Imgproc.resize(rgb_mat, rgb_mat, new Size(sr_force_size, sr_force_size));
        } catch (OrtException e) {
            e.printStackTrace();
        }

        return rgb_mat;

    }



}
