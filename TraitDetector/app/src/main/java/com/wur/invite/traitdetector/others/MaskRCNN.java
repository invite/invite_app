// Copyright (c) 2020 Facebook, Inc. and its affiliates.
// All rights reserved.
//
// This source code is licensed under the BSD-style license found in the
// LICENSE file in the root directory of this source tree.

package com.wur.invite.traitdetector.others;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.wur.invite.traitdetector.common.FileFinder;
import com.wur.invite.traitdetector.config.Config;
import com.wur.invite.traitdetector.processing.ImageOps;
import com.wur.invite.traitdetector.processing.Instance;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.pytorch.IValue;
import org.pytorch.LiteModuleLoader;
import org.pytorch.Module;
import org.pytorch.Tensor;
import org.pytorch.torchvision.TensorImageUtils;

import java.io.IOException;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

;


public class MaskRCNN {
    // for yolov5 model, no need to apply MEAN and STD
    public final static float[] NO_MEAN_RGB = new float[]{0.0f, 0.0f, 0.0f};
    public final static float[] NO_STD_RGB = new float[]{1.0f, 1.0f, 1.0f};

    // model input image size
    public final static int INPUT_WIDTH = 1920;
    public final static int INPUT_HEIGHT = 1080;
    public final static int OUTPUT_COLUMN = 6; // left, top, right, bottom, score and label
    static int MASK_SIZE = 28;
    public static String[] mClasses;
    private static Module model = null;
    private static Config user_cfg = null;





    public static void initModel(Context context, Config user_cfg) {
        // Load model
        try {
            model = LiteModuleLoader.load(FileFinder.assetFilePath(context, user_cfg.maskrcnn_model_filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("APP", "Loaded model!");

        // Define classes
        List<String> classes = new ArrayList<>();
        classes.add("tomato");
        MaskRCNN.mClasses = new String[classes.size()];
        classes.toArray(MaskRCNN.mClasses);
    }


    public static Tensor preprocess(Mat img){

        // Resizing
        Mat resizedMat = new Mat();
        Imgproc.resize(img, resizedMat, new Size(INPUT_WIDTH, INPUT_HEIGHT));

        // Mat to bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(INPUT_WIDTH, INPUT_HEIGHT, Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(resizedMat, resizedBitmap);

        // Convert to tensor
        int bufferSize = 3 * resizedBitmap.getWidth() * resizedBitmap.getHeight();
        final FloatBuffer floatBuffer = Tensor.allocateFloatBuffer(bufferSize);
        TensorImageUtils.bitmapToFloatBuffer(resizedBitmap, 0, 0, resizedBitmap.getWidth(), resizedBitmap.getHeight(), MaskRCNN.NO_MEAN_RGB, MaskRCNN.NO_STD_RGB, floatBuffer, 0);
        Tensor inputTensor = Tensor.fromBlob(floatBuffer, new long[]{3, resizedBitmap.getHeight(), resizedBitmap.getWidth()});

        return inputTensor;
    }


    public static List<Instance> process_image(Mat img) {

        Tensor inputTensor = preprocess(img);

        // Perform inference
        IValue[] outputTuple = model.forward(IValue.listFrom(inputTensor)).toTuple();


        final Map<String, IValue> map = outputTuple[1].toList()[0].toDictStringKey();
        float[] boxesData;
        float[] scoresData;
        long[] labelsData;
        float[] masksData;
        int count = 0;


        List<Instance> instancesList = new ArrayList<>();
        if (map.containsKey("boxes")) {
            final Tensor boxesTensor = map.get("boxes").toTensor();
            final Tensor scoresTensor = map.get("scores").toTensor();
            final Tensor labelsTensor = map.get("labels").toTensor();
            final Tensor masksTensor = map.get("masks").toTensor();
            boxesData = boxesTensor.getDataAsFloatArray();
            scoresData = scoresTensor.getDataAsFloatArray();
            labelsData = labelsTensor.getDataAsLongArray();
            masksData = masksTensor.getDataAsFloatArray();
            count = labelsData.length;

            for (int i = 0; i < count; i++) {

                float[] b = {boxesData[4 * i], boxesData[4 * i + 1], boxesData[4 * i + 2], boxesData[4 * i + 3]};
                float[] m = new float[28 * 28];
                for (int j = 0; j < 28 * 28; j++) {
                    m[j] = (masksData[(i * 28 * 28) + j] * 255);
                }
                float s = scoresData[i];
                int c = (int) (labelsData[i] - 1);
                Instance instance = new Instance(c, b, s, m);
                instancesList.add(instance);



                // Reshape the mask into a mat
                Mat mask_output = new Mat(MASK_SIZE, MASK_SIZE, CvType.CV_32F);
                mask_output.put(0, 0, m);

                // Scale the boxes according to mask size
                float[] scale_box = b.clone();
                ImageOps.rescale_boxes(scale_box, img.width() , img.height(), MASK_SIZE, MASK_SIZE);

                // Prepare a mask map with 0s
                Mat mask_map = new Mat(img.height(), img.width() , CvType.CV_8U, Scalar.all(0));

                int blursize1 = (int) img.width() / MASK_SIZE;
                int blursize2 = (int) img.height() / MASK_SIZE;
                int scale_x1 = (int) scale_box[0];
                int scale_y1 = (int) scale_box[1];
                int scale_x2 = (int) scale_box[2];
                int scale_y2 = (int) scale_box[3];
                int x1 = (int) instance.b[0];
                int y1 = (int) instance.b[1];
                int x2 = (int) instance.b[2];
                int y2 = (int) instance.b[3];

                // Get the mask from a region of the mask output
                Rect region = new Rect(scale_x1, scale_y1, scale_x2 - scale_x1, scale_y2 - scale_y1);
                Mat scale_crop_mask = new Mat(mask_output, region);

                // Resize the mask according to the actual image size
                Mat crop_mask = new Mat();
                Imgproc.resize(scale_crop_mask, crop_mask, new Size(x2 - x1, y2 - y1));

                // Blur the cropped mask
//                Imgproc.blur(crop_mask, crop_mask, new Size(blursize1, blursize2));

                // Get pixels that are above 0.5
//                for (int i = 0; i < crop_mask.rows(); i++) {
//                    for (int j = 0; j < crop_mask.cols(); j++) {
//                        double v = crop_mask.get(i, j)[0];
//                        if (v > 0.5)
//                            crop_mask.put(i, j, 1);
//                        else
//                            crop_mask.put(i, j, 0);
//                    }
//                }
                crop_mask.convertTo(crop_mask, CvType.CV_8U, 255);

                // Put the submat to the mask map
                Mat dest_mat = mask_map.submat(y1, y2, x1, x2);
                crop_mask.copyTo(dest_mat);

                instance.m_map = mask_map;
            }
        }

        return instancesList;
    }

    /*
    public static Result outputsToPredictions(int countResult,
                                              ArrayList<float[]> output_masks,
                                              float[] outputs,
                                              float[] fovS,
                                              float depth) {

        // Calculate the image width in mm using the field of view
        float Tx = (float) (2 * Math.tan(0.5 * fovS[0]) * depth);
        float Ty = (float) (2 * Math.tan(0.5 * fovS[1]) * depth);
        float pixPerMmX = Tx / 1920;
        float pixPerMmY = Ty / 1080;

        // Store all outputs in an instance list
        ArrayList<Instance> instances = new ArrayList<>();

        for (int i = 0; i < countResult; i++) {
            Instance instance = new Instance();

            // Determine the chunk of the output based on the number of the iteration
            // and the total number of output columns.
            int chunk = i * OUTPUT_COLUMN;

            instance.setConfidence(outputs[chunk + 4]);
            instance.setClassIndex((int) outputs[chunk + 5]);
            instance.setMask(output_masks.get(i));

            float x1 = outputs[chunk];
            float y1 = outputs[chunk + 1];
            float x2 = outputs[chunk + 2];
            float y2 = outputs[chunk + 3];

            instance.calculateDimensions(x1, y1, x2, y2, pixPerMmX, pixPerMmY);

            instances.add(instance);
        }

        ArrayList<Instance> upCluster = new ArrayList<>();
        ArrayList<Instance> sideCluster = new ArrayList<>();
        ArrayList<Instance> downCluster = new ArrayList<>();

        instances.sort(Comparator.comparing(Instance::getCentroidY));

        float topLowerBound = instances.get(0).getBounds()[3];
        float bottomUpperBound = instances.get(countResult - 1).getBounds()[1];
        float distanceLimit = 30 / pixPerMmY;

        for (Instance instance : instances) {
            float instanceUpperBound = instance.getBounds()[1];
            float instanceLowerBound = instance.getBounds()[3];
            float instanceUpperDistance = instanceUpperBound - topLowerBound;
            float instanceLowerDistance = bottomUpperBound - instanceLowerBound;

            if (instanceUpperDistance < distanceLimit) {
                instance.setClassIndex(0);
                upCluster.add(instance);
            } else if (instanceLowerDistance < distanceLimit) {
                instance.setClassIndex(1);
                downCluster.add(instance);
            } else {
                instance.setClassIndex(2);
                sideCluster.add(instance);
            }
        }

        int orientationCount = Math.min(Math.min(upCluster.size(), sideCluster.size()), downCluster.size());

        ArrayList<Float> diametersMm = new ArrayList<>();
        ArrayList<Float> heightMm = new ArrayList<>();
        ArrayList<Double> volumesMm = new ArrayList<>();
        ArrayList<Float> ratiosMm = new ArrayList<>();

        for (int i=0; i < orientationCount; i++) {
            Instance up = upCluster.get(i);
            Instance side = sideCluster.get(i);
            Instance down = downCluster.get(i);

            // Get x1 and y1 from top
            float x1 = up.getWidthMm();
            float y1 = up.getHeightMm();

            // Get x2 and z1 from side
            float x2 = side.getWidthMm();
            float z1 = side.getHeightMm();

            // Get x3 and y2 from down
            float x3 = down.getWidthMm();
            float y2 = down.getHeightMm();

            float xAvg = (x1 + x2 + x3) / 3;
            float yAvg = (y1 + y2) / 2;
            float zAvg = z1;
            float diameterAvg = (xAvg + yAvg) / 2;

            diametersMm.add(diameterAvg);
            heightMm.add(zAvg);
            volumesMm.add((4.0/3.0)*xAvg*yAvg*zAvg*Math.PI);
            ratiosMm.add(zAvg / diameterAvg);
        }

        double avgDiameter = diametersMm.stream().mapToDouble(a -> a).average().orElse(-1);
        double avgHeight = heightMm.stream().mapToDouble(a -> a).average().orElse(-1);;
        double avgVolume = volumesMm.stream().mapToDouble(a -> a).average().orElse(-1);;
        double avgRatio = ratiosMm.stream().mapToDouble(a -> a).average().orElse(-1);;

        Result result = new Result(instances, (float) avgDiameter, (float) avgHeight, (float) avgVolume, (float) avgRatio);

        return result;
    }

    */
}