package com.wur.invite.traitdetector.processing;
import com.google.ar.core.Camera;
import com.google.ar.core.CameraIntrinsics;
import com.wur.invite.traitdetector.config.Config;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;


import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class ImageOps {

    public static void displayImage(Mat img, ImageView imageView){
        Mat img_clone = img.clone();
        Imgproc.cvtColor(img_clone, img_clone, Imgproc.COLOR_BGR2RGB);
        Bitmap bitmap_output = null;
        bitmap_output = Bitmap.createBitmap(img_clone.width(), img_clone.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(img_clone, bitmap_output);
        imageView.setImageBitmap(bitmap_output);
    }

    public static int getMax(int[] inputArray){
        int maxValue = inputArray[0];
        for(int i=1;i < inputArray.length;i++){
            if(inputArray[i] > maxValue){
                maxValue = inputArray[i];
            }
        }
        return maxValue;
    }

    public static int getMin(int[] inputArray){
        int minValue = inputArray[0];
        for(int i=1;i<inputArray.length;i++){
            if(inputArray[i] < minValue){
                minValue = inputArray[i];
            }
        }
        return minValue;
    }

    public static Mat resizeWithPadding(Mat src, int width, int height) {
        Mat dst = new Mat();
        int oldW = src.width();
        int oldH = src.height();

        double r = Math.min((double) width / oldW, (double) height / oldH);

        int newUnpadW = (int) Math.round(oldW * r);
        int newUnpadH = (int) Math.round(oldH * r);

        int dw = (width - newUnpadW) / 2;
        int dh = (height - newUnpadH) / 2;

        int top = (int) Math.round(dh - 0.1);
        int bottom = (int) Math.round(dh + 0.1);
        int left = (int) Math.round(dw - 0.1);
        int right = (int) Math.round(dw + 0.1);

        Imgproc.resize(src, dst, new Size(newUnpadW, newUnpadH));
        Core.copyMakeBorder(dst, dst, top, bottom, left, right, Core.BORDER_CONSTANT);

        return dst;
    }

    public static void resizeWithPadding(Mat src, Mat dst, int width, int height) {
        int oldW = src.width();
        int oldH = src.height();

        double r = Math.min((double) width / oldW, (double) height / oldH);

        int newUnpadW = (int) Math.round(oldW * r);
        int newUnpadH = (int) Math.round(oldH * r);

        int dw = (width - newUnpadW) / 2;
        int dh = (height - newUnpadH) / 2;

        int top = (int) Math.round(dh - 0.1);
        int bottom = (int) Math.round(dh + 0.1);
        int left = (int) Math.round(dw - 0.1);
        int right = (int) Math.round(dw + 0.1);

        Imgproc.resize(src, dst, new Size(newUnpadW, newUnpadH));
        Core.copyMakeBorder(dst, dst, top, bottom, left, right, Core.BORDER_CONSTANT);
    }

    public static void whc2cwh(float[] src, float[] dst, int start) {
        int j = start;
        for (int ch = 0; ch < 3; ++ch) {
            for (int i = ch; i < src.length; i += 3) {
                dst[j] = src[i];
                j++;
            }
        }
    }

    public static float[] whc2cwh(float[] src) {
        float[] chw = new float[src.length];
        int j = 0;
        for (int ch = 0; ch < 3; ++ch) {
            for (int i = ch; i < src.length; i += 3) {
                chw[j] = src[i];
                j++;
            }
        }
        return chw;
    }

    public static byte[] whc2cwh(byte[] src) {
        byte[] chw = new byte[src.length];
        int j = 0;
        for (int ch = 0; ch < 3; ++ch) {
            for (int i = ch; i < src.length; i += 3) {
                chw[j] = src[i];
                j++;
            }
        }
        return chw;
    }

    public static int argmax(float[] a) {
        float re = -Float.MAX_VALUE;
        int arg = -1;
        for (int i = 0; i < a.length; i++) {
            if (a[i] >= re) {
                re = a[i];
                arg = i;
            }
        }
        return arg;
    }

    public static void xywh2xyxy(float[] bbox) {
        float x = bbox[0];
        float y = bbox[1];
        float w = bbox[2];
        float h = bbox[3];

        bbox[0] = x - w * 0.5f;
        bbox[1] = y - h * 0.5f;
        bbox[2] = x + w * 0.5f;
        bbox[3] = y + h * 0.5f;
    }

    public static void rescale_boxes(float[] bbox, int s1x, int s1y, int s2x, int s2y) {
        // xmin, ymin, xmax, ymax -> (xmin_org, ymin_org, xmax_org, ymax_org)
        bbox[0] = bbox[0] / s1x * s2x;
        bbox[1] = bbox[1] / s1y * s2y;
        bbox[2] = bbox[2] / s1x * s2x;
        bbox[3] = bbox[3] / s1y * s2y;
    }

    public static List<float[]> nms(List<float[]> bboxes, float iouThreshold) {

        // output boxes
        List<float[]> bestBboxes = new ArrayList<>();

        // confidence
        bboxes.sort(Comparator.comparing(a -> a[4]));

        // standard nms
        while (!bboxes.isEmpty()) {
            float[] bestBbox = bboxes.remove(bboxes.size() - 1);
            bestBboxes.add(bestBbox);
            bboxes = bboxes.stream().filter(a -> ImageOps.iou(a, bestBbox) < iouThreshold).collect(Collectors.toList());
        }

        return bestBboxes;
    }

    public static float iou(float[] box1, float[] box2) {

        float area1 = (box1[2] - box1[0]) * (box1[3] - box1[1]);
        float area2 = (box2[2] - box2[0]) * (box2[3] - box2[1]);

        float left = Math.max(box1[0], box2[0]);
        float top = Math.max(box1[1], box2[1]);
        float right = Math.min(box1[2], box2[2]);
        float bottom = Math.min(box1[3], box2[3]);

        float interArea = Math.max(right - left, 0) * Math.max(bottom - top, 0);
        float unionArea = area1 + area2 - interArea;
        return Math.max(interArea / unionArea, 1e-8f);

    }

    public static int get_max_contour(List<MatOfPoint> contours){
        double maxVal = 0;
        int maxValIdx = 0;
        for (int contourIdx = 0; contourIdx < contours.size(); contourIdx++)
        {
            double contourArea = Imgproc.contourArea(contours.get(contourIdx));
            if (maxVal < contourArea)
            {
                maxVal = contourArea;
                maxValIdx = contourIdx;
            }
        }
        return maxValIdx;
    }

    public static int euclidean_distance(
            int x1,
            int y1,
            int x2,
            int y2) {
        return (int) Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
    }





    public static float[] get_object_dimensions(Mat m_map){

        m_map.convertTo(m_map, CvType.CV_32SC1, 255);
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(m_map, contours, new Mat(), Imgproc.RETR_FLOODFILL, Imgproc.CHAIN_APPROX_SIMPLE);
        Point[] max_contour = contours.get(get_max_contour(contours)).toArray();

        MatOfPoint2f areaPoints = new MatOfPoint2f(max_contour);
        Rect boundingRect = Imgproc.boundingRect(areaPoints);

        // Based on contours
        float width = boundingRect.width;
        float height = boundingRect.height;

        return new float[]{width, height};
    }

    public static Mat draw_instances_tomato(Mat img, List<Instance> instancesList, Config myConfig){
        Mat dest_img = img.clone();

        String[] orientation_texts = {"Top", "Side", "Bottom"};
        int[][] color_ints = myConfig.colors;
        ArrayList<Scalar> colors = new ArrayList<Scalar>();
        for (int[] color_int : color_ints) {
            colors.add(new Scalar(new double[]{color_int[0], color_int[1], color_int[2]}));
        }

        for (Instance instance : instancesList) {
            float[] b = instance.b;
            float[] b2 = instance.b2;
            int index = instance.index;
            int c = instance.c;
            int c2 = instance.c2;
            int orientation = instance.orientation;
            int w = (int)instance.width;
            int h = (int)instance.height;
            float w_mm = (float)instance.widthMm;
            float h_mm = (float)instance.heightMm;
            int w2 = (int)instance.width_2;
            int h2 = (int)instance.height_2;
            float w2_mm = (float)instance.widthMm_2;
            float h2_mm = (float)instance.heightMm_2;

            float distance = (float)instance.distance;



            Log.d("INVITE", "C1:" + c + " C2:" + c2 + " O:" + orientation_texts[orientation]);


            // Show box
            if (myConfig.draw_boxes == 1) {
                Imgproc.rectangle(dest_img,
                        new Point(b[0], b[1]),
                        new Point(b[2], b[3]),
                        colors.get(c),
                        myConfig.thickness
                );
            }

            // Show label

            if (myConfig.draw_labels == 1) {
                DecimalFormat df = new DecimalFormat("#.#");
//                    draw_text(dest_img,
//                            c + " (" + df.format(instance.s) + ")",
//                            new Point(bbox[0] - 1, bbox[1] - 45),
//                            colors.get(c),
//                            myConfig);


//                draw_text(dest_img,
//                        "D: " + df.format(distance) + "mm",
//                        new Point(b[0] - 1, b[1] - 65),
//                        colors.get(c),
//                        myConfig);
//
//                draw_text(dest_img,
//                        "W: " + df.format(w_mm) + "mm",
//                        new Point(b[0] - 1, b[1] - 35),
//                        colors.get(c),
//                        myConfig);
//
//                draw_text(dest_img,
//                        "H: " + df.format(h_mm) + "mm",
//                        new Point(b[0] - 1, b[1] - 5),
//                        colors.get(c),
//                        myConfig);

                draw_text(dest_img,
                        "[" + index + "] " + orientation_texts[orientation],
                        new Point(b[0] - 1, b[1] - 65),
                        colors.get(c),
                        myConfig);

                draw_text(dest_img,
                        "T: " + df.format(h_mm) + "mm x" + df.format(w_mm) + "mm",
                        new Point(b[0] - 1, b[1] - 35),
                        colors.get(c),
                        myConfig);


                draw_text(dest_img,
                        "P: " + df.format(h2_mm) + "mm x" + df.format(w2_mm) + "mm",
                        new Point(b[0] - 1, b[1] - 5),
                        colors.get(c),
                        myConfig);




            }

            Mat draw_m_map = new Mat();
            int draw_color = 0;

            if (myConfig.draw_contours == 1) {
                List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

                draw_m_map = instance.m_map;
                draw_m_map.convertTo(draw_m_map, CvType.CV_32SC1, 255);
                Imgproc.findContours(draw_m_map, contours, new Mat(), Imgproc.RETR_FLOODFILL, Imgproc.CHAIN_APPROX_SIMPLE);
                Imgproc.drawContours(dest_img, contours, ImageOps.get_max_contour(contours), colors.get(c), myConfig.thickness);


                contours = new ArrayList<MatOfPoint>();
                if (c2 == 1)
                {
                    draw_m_map = instance.m_map2;
                    draw_m_map.convertTo(draw_m_map, CvType.CV_32SC1, 255);
                    Imgproc.findContours(draw_m_map, contours, new Mat(), Imgproc.RETR_FLOODFILL, Imgproc.CHAIN_APPROX_SIMPLE);
                    Imgproc.drawContours(dest_img, contours, ImageOps.get_max_contour(contours), colors.get(c2), myConfig.thickness);
                }

            }


            int x1_dest = (int) ((int) b[0] * 0.98f);
            int y1_dest = (int) ((int) b[1] * 0.98f);
            int x2_dest = (int) ((int) b[2] * 1.02f);
            int y2_dest = (int) ((int) b[3] * 1.02f);

            Mat dest_mat = dest_img.submat(y1_dest, y2_dest, x1_dest, x2_dest);
            instance.img_output = dest_mat;


        }
        return dest_img;
    }

    public static void draw_text(Mat img, String text, Point thisPoint, Scalar color, Config myConfig){
        Imgproc.putText(
                img,
                text,
                thisPoint,
                Imgproc.FONT_HERSHEY_PLAIN,
                myConfig.font_scale,
                color,
                myConfig.thickness);
    }

    public static Mat draw_instances(Mat img, List<Instance> instancesList, Config myConfig){
        Mat dest_img = img.clone();


        int[][] color_ints = myConfig.colors;
        ArrayList<Scalar> colors = new ArrayList<Scalar>();
        for (int[] color_int : color_ints) {
            colors.add(new Scalar(new double[]{color_int[0], color_int[1], color_int[2]}));
        }

        for (Instance instance : instancesList) {
            float[] bbox = instance.b;
            int c = instance.c;



            // Show box
            if (myConfig.draw_boxes == 1) {
                Imgproc.rectangle(dest_img,
                        new Point(bbox[0], bbox[1]),
                        new Point(bbox[2], bbox[3]),
                        colors.get(c),
                        myConfig.thickness
                );
            }

            // Show label
            if (myConfig.draw_labels == 1) {
                DecimalFormat df = new DecimalFormat("#.##");
                Imgproc.putText(
                        dest_img,
                        c + " (" + df.format(instance.s) + ")",
                        new Point(bbox[0] - 1, bbox[1] - 5),
                        Imgproc.FONT_HERSHEY_SIMPLEX,
                        myConfig.font_scale, colors.get(c),
                        myConfig.thickness);
            }

            // Show contours
            if (myConfig.draw_contours == 1) {

                Mat m_map = instance.m_map;
                m_map.convertTo(m_map, CvType.CV_32SC1, 255);
                List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
                Imgproc.findContours(m_map, contours, new Mat(), Imgproc.RETR_FLOODFILL, Imgproc.CHAIN_APPROX_SIMPLE);
                for (int i = 0; i < contours.size(); i++) {
                    Imgproc.drawContours(dest_img, contours, i, colors.get(c), myConfig.thickness);
                }
            }


        }
        return dest_img;
    }

    public static Bitmap display_output(Mat img, List<Instance> detectionList, String filename, Config myConfig) {
        int[][] color_ints = myConfig.colors;

        ArrayList<Scalar> colors = new ArrayList<Scalar>();
        for (int[] color_int : color_ints) {
            colors.add(new Scalar(new double[]{color_int[0], color_int[1], color_int[2]}));
        }



        Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2RGB);
        for (Instance instance : detectionList) {
            float[] bbox = instance.b;
            int c = instance.c;


            // Show box
            if (myConfig.draw_boxes == 1) {
                Imgproc.rectangle(img,
                        new Point(bbox[0], bbox[1]),
                        new Point(bbox[2], bbox[3]),
                        colors.get(c),
                        myConfig.thickness
                );
            }

            // Show label
            if (myConfig.draw_labels == 1) {
                DecimalFormat df = new DecimalFormat("#.##");
                Imgproc.putText(
                        img,
                        c + " (" + df.format(instance.s) + ")",
                        new Point(bbox[0] - 1, bbox[1] - 5),
                        Imgproc.FONT_HERSHEY_SIMPLEX,
                        myConfig.font_scale, colors.get(c),
                        myConfig.thickness);
            }

            // Show contours
            if (myConfig.draw_contours == 1) {
                Mat m_map = instance.m_map;
                m_map.convertTo(m_map, CvType.CV_32SC1, 255);
                List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
                Imgproc.findContours(m_map, contours, new Mat(), Imgproc.RETR_FLOODFILL, Imgproc.CHAIN_APPROX_SIMPLE);
                for (int i = 0; i < contours.size(); i++) {
                    Imgproc.drawContours(img, contours, i, colors.get(c), myConfig.thickness);
                }
            }


            String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/Invite/test.jpg";
            File file = new File(path);
            filename = file.toString();
            Imgcodecs.imwrite(filename, img);
        }

        Bitmap bitmap_output = null;
        bitmap_output = Bitmap.createBitmap(img.width(), img.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(img, bitmap_output);

        return bitmap_output;
    }


}
