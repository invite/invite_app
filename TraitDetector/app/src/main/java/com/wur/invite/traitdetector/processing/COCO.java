package com.wur.invite.traitdetector.processing;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class COCO {

    public static void write_coco_json(String filename, Mat img, List<Instance> all_objects, Context context) throws JSONException {


        SimpleDateFormat date_formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        SimpleDateFormat year_formatter = new SimpleDateFormat("yyyy");
        Date date = new Date();
        String date_string = date_formatter.format(date);
        String year_string = year_formatter.format(date);

        JSONObject writedata = new JSONObject();
        JSONObject info = new JSONObject();
        JSONObject licenses = new JSONObject();
        JSONArray images = new JSONArray();
        JSONObject type = new JSONObject();
        JSONArray annotations = new JSONArray();
        JSONObject categories = new JSONObject();

        info.put("description", "description");
        info.put("url", "url");
        info.put("version", 1);
        info.put("year", year_string);
        info.put("contributor", "contributor");
        info.put("date_created", date_string);
        licenses.put("url", "license_url");
        licenses.put("id", "license_id");
        licenses.put("name", "license_name");

        writedata.put("info", info);
        writedata.put("licenses", licenses);
        writedata.put("images", images);
        writedata.put("type", "instances");
        writedata.put("categories", categories);


        int w = img.width();
        int h = img.height();


        JSONObject img_info = new JSONObject();

        img_info.put("license", 0);
        img_info.put("url", 0);
        img_info.put("file_name", filename);
        img_info.put("license", 0);
        img_info.put("height", h);
        img_info.put("width", w);
        img_info.put("date_captured", date_string);
        img_info.put("id", 1);
        images.put(img_info);


        for (Instance o : all_objects) {

            JSONObject annotation = new JSONObject();


            JSONArray box_json = new JSONArray();
            JSONArray polygon_json = new JSONArray();
            List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
            Point[] max_contour = new Point[0];

            for (float bb : o.b){
                box_json.put(bb);
            }

            polygon_json = new JSONArray();
            contours = new ArrayList<MatOfPoint>();
            o.m_map.convertTo(o.m_map, CvType.CV_32SC1, 255);
            Imgproc.findContours(o.m_map, contours, new Mat(), Imgproc.RETR_FLOODFILL, Imgproc.CHAIN_APPROX_SIMPLE);
            max_contour = contours.get(ImageOps.get_max_contour(contours)).toArray();
            for (Point pp : max_contour)
            {
                polygon_json.put((float)pp.x);
                polygon_json.put((float)pp.y);
            }

            annotation.put("id", o.index);
            annotation.put("image_id", 1);
            annotation.put("category_id", o.c);
            annotation.put("keypoints", "");
            annotation.put("num_keypoints", "");
            annotation.put("segmentation", polygon_json);
            annotation.put("area", 0);
            annotation.put("bbox", box_json);
            annotation.put("distance", o.distance);
            annotation.put("area", 0);
            annotation.put("iscrowd", 0);
            annotations.put(annotation);

            if (o.c2 == 1)
            {
                annotation = new JSONObject();
                box_json = new JSONArray();
                for (float bb : o.b2){
                    box_json.put(bb);
                }
                polygon_json = new JSONArray();

                o.m_map2.convertTo(o.m_map2, CvType.CV_32SC1, 255);
                Imgproc.findContours(o.m_map2, contours, new Mat(), Imgproc.RETR_FLOODFILL, Imgproc.CHAIN_APPROX_SIMPLE);
                max_contour = contours.get(ImageOps.get_max_contour(contours)).toArray();
                for (Point pp : max_contour)
                {
                    polygon_json.put((float)pp.x);
                    polygon_json.put((float)pp.y);
                }

                annotation.put("id", 100 + o.index);
                annotation.put("image_id", 1);
                annotation.put("category_id", o.c2);
                annotation.put("keypoints", "");
                annotation.put("num_keypoints", "");
                annotation.put("segmentation", polygon_json);
                annotation.put("area", 0);
                annotation.put("bbox", box_json);
                annotation.put("distance", o.distance);
                annotation.put("area", 0);
                annotation.put("iscrowd", 0);
                annotations.put(annotation);
            }



        }

        String basename = filename.split("\\.(?=[^\\.]+$)")[0];
        writedata.put("annotations", annotations);
        File internalPath = new File(context.getFilesDir().getPath() + "/trial_data/" + basename);
        if (!internalPath.exists()) {
            internalPath.mkdirs();
        }
        try {
            FileOutputStream fileout = new FileOutputStream(new File(internalPath, "data.json"));
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileout);
            outputWriter.write(writedata.toString());
            outputWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


