package com.wur.invite.traitdetector.processing;

import android.util.Log;

import ai.onnxruntime.NodeInfo;
import ai.onnxruntime.OrtEnvironment;
import ai.onnxruntime.OrtException;
import ai.onnxruntime.OrtSession;

public class ORT {

    public static class ONNXModel{
        public OrtEnvironment env;
        public OrtSession session;
        public String input_name;

        public ONNXModel(String model_filename)  {
            OrtEnvironment env = OrtEnvironment.getEnvironment();
            OrtSession session = null;

            OrtSession.SessionOptions session_options = new OrtSession.SessionOptions();

            if (model_filename.contains(".ort")) {
                try {
                    session_options.addConfigEntry("session.load_model_format", "ORT");
                } catch (OrtException e) {
                    e.printStackTrace();
                }
            }
            try {
                session = env.createSession(model_filename, session_options);
            } catch (OrtException e) {
                Log.d("ONNX", String.valueOf(e));
                e.printStackTrace();
            }
            assert session != null;
            String input_name = session.getInputNames().iterator().next();

            this.env = env;
            this.session = session;
            this.input_name = input_name;
            ORT.checkModel(this.session);
        }
    }

    public static void checkModel(OrtSession session) {
        Log.d("ONNX", "Inputs:");
        try {
            for (NodeInfo i : session.getInputInfo().values()) {
                Log.d("ONNX", i.toString());
            }
        } catch (
                OrtException e) {
            e.printStackTrace();
        }
        Log.d("ONNX", "Outputs:");
        try {
            for (NodeInfo i : session.getOutputInfo().values()) {
                Log.d("ONNX", i.toString());
            }
        } catch (OrtException e) {
            e.printStackTrace();
        }
    }

}
