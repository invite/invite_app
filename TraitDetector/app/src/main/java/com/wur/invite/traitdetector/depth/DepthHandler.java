package com.wur.invite.traitdetector.depth;

import static android.opengl.GLES20.GL_CLAMP_TO_EDGE;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TEXTURE_MAG_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_MIN_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_WRAP_S;
import static android.opengl.GLES20.GL_TEXTURE_WRAP_T;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glGenTextures;
import static android.opengl.GLES20.glTexParameteri;
import static android.opengl.GLES30.GL_LINEAR;


import static java.lang.Integer.parseInt;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.media.Image;
import android.os.Environment;
import android.util.Log;


import com.google.ar.core.Camera;
import com.google.ar.core.CameraIntrinsics;
import com.google.ar.core.Frame;
import com.wur.invite.traitdetector.config.Config;
import com.wur.invite.traitdetector.processing.COCO;
import com.wur.invite.traitdetector.processing.ImageOps;
import com.wur.invite.traitdetector.processing.Instance;
import com.wur.invite.traitdetector.processing.Processor;
import com.wur.invite.traitdetector.processing.Traits;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Dictionary;
import java.util.List;
import java.util.Random;

/**
 * Handle RG8 GPU texture containing a DEPTH16 depth image.
 */
public final class DepthHandler {

    private int depthTextureId = -1;
    private int depthTextureWidth = -1;
    private int depthTextureHeight = -1;

    private float mImgScaleX, mImgScaleY, mIvScaleX, mIvScaleY;

    /**
     * Creates and initializes the depth texture. This method needs to be called on a
     * thread with a EGL context attached.
     */
    public void createOnGlThread() {
        int[] textureId = new int[1];
        glGenTextures(1, textureId, 0);
        depthTextureId = textureId[0];
        glBindTexture(GL_TEXTURE_2D, depthTextureId);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }

    /**
     * Updates the depth texture with the content from acquireDepthImage().
     * This method needs to be called on a thread with an EGL context attached.
     */
    public void update(final Frame frame) {

    }

    public JSONObject saveDepth(ARActivity context, final Frame frame, JSONObject qrData, Image cameraImage, int pixelStride, int rowStride, ByteBuffer depthBufferRaw, int[] depth_shape) throws JSONException {
        JSONObject traits_json  = new JSONObject();

        Log.d("SAVE DEPTH", "STARTING");

        try {
            //
            // Get FOV
            //
            Camera camera = frame.getCamera();
            CameraIntrinsics imageIntrinsics = camera.getImageIntrinsics();
            float[] focalLength = imageIntrinsics.getFocalLength();
            int[] imageDimensions = imageIntrinsics.getImageDimensions();
            double fovW = 2 * Math.atan(imageDimensions[0] / (focalLength[0] * 2.0));
            double fovH = 2 * Math.atan(imageDimensions[1] / (focalLength[0] * 2.0));
            float[] fov = {(float) fovW, (float) fovH};



            //
            // Get trial info
            //
            String year = qrData.getString("year");
            String date = qrData.getString("date");
            SimpleDateFormat input = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date extracted_date = input.parse(date);
            SimpleDateFormat output = new SimpleDateFormat("yyyyMMddHHmm");
            String date_for_filename = output.format(extracted_date);

            String trial = qrData.getString("trial");
            String id = qrData.getString("id");
            String varietyCode = date_for_filename + "_" + trial + "_" + id;

            // Pictures path
            String picturesPath = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES).toString();
            File trialDataPath = new File(picturesPath + "/Invite/" + varietyCode);
            if (!trialDataPath.exists()) {
                trialDataPath.mkdirs();
            }
            final File out = new File(trialDataPath.getAbsolutePath(), "raw.png");


            // Internal path
            File internalPath = new File(context.getFilesDir().getPath() + "/trial_data/" + varietyCode);
            if (!internalPath.exists()) {
                internalPath.mkdirs();
            }
            final File outX = new File(internalPath.getAbsolutePath(), "raw.png");



            //
            // Pre-process image
            //
            byte[] nv21;
            ByteBuffer yBuffer = cameraImage.getPlanes()[0].getBuffer();
            ByteBuffer uBuffer = cameraImage.getPlanes()[1].getBuffer();
            ByteBuffer vBuffer = cameraImage.getPlanes()[2].getBuffer();
            int ySize = yBuffer.remaining();
            int uSize = uBuffer.remaining();
            int vSize = vBuffer.remaining();
            nv21 = new byte[ySize + uSize + vSize];

            //U and V are swapped
            yBuffer.get(nv21, 0, ySize);
            vBuffer.get(nv21, ySize, vSize);
            uBuffer.get(nv21, ySize + vSize, uSize);

            int width = cameraImage.getWidth();
            int height = cameraImage.getHeight();

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            YuvImage yuv = new YuvImage(nv21, ImageFormat.NV21, width, height, null);
            yuv.compressToJpeg(new Rect(0, 0, width, height), 100, outputStream);
            byte[] byteArray = outputStream.toByteArray();
            Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

            if (bitmap.getHeight() > bitmap.getWidth()) {
                Matrix matrix = new Matrix();
                matrix.postRotate(270);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            }


            // Write image to disk
            FileOutputStream fos = new FileOutputStream(out);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
//            FileOutputStream fos2 = new FileOutputStream(outX);
//            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos2);
//            fos2.flush();
//            fos2.close();
            cameraImage.close();



            //
            // Get image
            //
            Mat img = new Mat(bitmap.getHeight(), bitmap.getWidth(), CvType.CV_8UC3);
            Bitmap bmp32 = bitmap.copy(Bitmap.Config.ARGB_8888, true);
            Utils.bitmapToMat(bmp32, img);
            Imgproc.cvtColor(img, img, Imgproc.COLOR_RGBA2BGR);

            // Process the image
            //
            Config user_cfg = new Config();



            DepthData depth_data = new DepthData();
            depth_data.pixelStride = pixelStride;
            depth_data.rowStride = rowStride;
            depth_data.depthBufferRaw = depthBufferRaw;
            depth_data.depth_shape = depth_shape;


            // Process image
            List<Instance> all_objects = Processor.full_process_image(varietyCode, img, user_cfg, depth_data, fov, context);

            // Draw instances
            Mat img_output_final = ImageOps.draw_instances_tomato(img, all_objects, user_cfg);


            // Save image output
            Imgproc.cvtColor(img_output_final, img_output_final, Imgproc.COLOR_BGR2RGB);
            final File out2 = new File(trialDataPath.getAbsolutePath(), "output.png");
            FileOutputStream fos3 = new FileOutputStream(out2);
            Bitmap bitmap_output = Bitmap.createBitmap(img_output_final.width(), img_output_final.height(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(img_output_final, bitmap_output);
            bitmap_output.compress(Bitmap.CompressFormat.PNG, 100, fos3);
            fos3.flush();

            // Get individual objects
            File masksPath = new File(trialDataPath.getAbsolutePath() + "/masks/");
            Processor.extract_instances(all_objects, masksPath);




            Traits traits_data = new Traits();
            traits_data.measure_traits(all_objects);
            traits_json = qrData.getJSONObject("traits");
            traits_json = traits_data.write_trait_json();



//


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return traits_json;
    }















    public int getDepthTexture() {
        return depthTextureId;
    }
//
//
//    public float getDepthOnly(Image depthImage, int x, int y) {
//        Image.Plane depthPlane = depthImage.getPlanes()[0];
//        int depthIndex = x * depthPlane.getPixelStride() + y * depthPlane.getRowStride();
//        ByteBuffer depthBuffer = depthPlane.getBuffer().order(ByteOrder.nativeOrder());
//
//        return depthBuffer.getShort(depthIndex);
//    }
//
//
//
//    public float[] getDepthValue(Image depthImage, Image depthConfidence, int x, int y) {
//        Image.Plane depthPlane = depthImage.getPlanes()[0];
//        Image.Plane confidencePlane = depthConfidence.getPlanes()[0];
//
//        int depthIndex = x * depthPlane.getPixelStride() + y * depthPlane.getRowStride();
//        ByteBuffer depthBuffer = depthPlane.getBuffer().order(ByteOrder.nativeOrder());
//
//        int confidenceIndex = x * confidencePlane.getPixelStride() + y * confidencePlane.getRowStride();
//        ByteBuffer confidenceBuffer = confidencePlane.getBuffer().order(ByteOrder.nativeOrder());
//
//        float depth = depthBuffer.getShort(depthIndex);
//        short confidence = confidenceBuffer.getShort(confidenceIndex);
//        float confidenceNormalized = ((float) (confidence & 0xff)) / 255.0f;
//
//        return new float[] {depth, confidenceNormalized};
//    }
//
//    public int getDepthWidth() {
//        return depthTextureWidth;
//    }
//
//    public int getDepthHeight() {
//        return depthTextureHeight;
//    }
}