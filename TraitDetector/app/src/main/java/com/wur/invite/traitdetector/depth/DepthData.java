package com.wur.invite.traitdetector.depth;

import java.io.IOException;
import java.nio.ByteBuffer;

public class DepthData {
    public int pixelStride;
    public int rowStride;
    public ByteBuffer depthBufferRaw;
    public int[] depth_shape = {0, 0};
    public int distance;

    public void compute_distance(int[] centroid, int[] img_shape) throws IOException {
        float depth_scale_x = img_shape[0] / depth_shape[0];
        float depth_scale_y = img_shape[1] / depth_shape[1];

        int x = (int) (centroid[0] / depth_scale_x);
        int y = (int) (centroid[1] / depth_scale_y);

        int depthIndex = x * pixelStride + y * rowStride;

        ByteBuffer depthBuffer = ByteBuffer.allocate(depthBufferRaw.capacity());
        depthBufferRaw.rewind();
        depthBuffer.put(depthBufferRaw);
        depthBufferRaw.rewind();
        depthBuffer.flip();

        this.distance = depthBufferRaw.getShort(depthIndex);

    }
}
