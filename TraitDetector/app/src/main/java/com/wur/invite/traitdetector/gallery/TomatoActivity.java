package com.wur.invite.traitdetector.gallery;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wur.invite.traitdetector.R;
import com.wur.invite.traitdetector.trials.Trials;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TomatoActivity extends AppCompatActivity {

    private static final DecimalFormat floored = new DecimalFormat("0");
    private static final DecimalFormat df = new DecimalFormat("0.00");
    private RecyclerView cropped;
    private RecyclerView.Adapter adapter;
    private ArrayList<Cropped> croppedList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tomato_activity);

        this.cropped = findViewById(R.id.croppedView);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        this.cropped.setLayoutManager(mLayoutManager);


        ImageView imageView = findViewById(R.id.image);
        TextView labelView = findViewById(R.id.label);
//        TextView widthValue = findViewById(R.id.widthValue);
//        TextView sizeValue = findViewById(R.id.sizeValue);
//        TextView heightValue = findViewById(R.id.heightValue);
        TextView volumeValue = findViewById(R.id.volumeValue);
        TextView ratioValue = findViewById(R.id.ratioValue);
        TextView distanceValue = findViewById(R.id.distanceValue);
        TextView peduncleSizeValue = findViewById(R.id.peduncleSizeValue);

        String picturesPath = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES).toString();
        File trialDataPath = new File(picturesPath + "/Invite/");
        Intent intent = getIntent();

        try {
            JSONObject variety = new JSONObject(intent.getStringExtra("variety"));

            String year = variety.getString("year");
            String date = variety.getString("date");
            String trial = variety.getString("trial");
            int id = variety.getInt("id");
            String label = variety.getString("label");
            JSONObject traits = variety.getJSONObject("traits");








            SimpleDateFormat input = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date extracted_date = input.parse(date);
            SimpleDateFormat output = new SimpleDateFormat("yyyyMMddHHmm");
            String date_for_filename = output.format(extracted_date);
            String varietyCode = date_for_filename + "_" + trial + "_" + id;


            String imageURI = Trials.getTrialOutputImg(variety);

            labelView.setText(label);
            Drawable d = Drawable.createFromPath(imageURI);
            imageView.setImageDrawable(d);


//        Trait map:  26 Size mm2
//                    27 Ratio L/D
//                    28 Longitudinal shape
//                    30 Peduncle depression
//                    31 Peduncle Scar Size
//                    33 Blossom End Shape
//                    34 Diameter
//                    37 Color
//                    101 Height
//                    102 Volume


            //
            // Show cropped images
            //
            ArrayList<Cropped> cropped = new ArrayList<>();
            File maskDataPath = new File(trialDataPath.getAbsolutePath() + "/" + varietyCode + "/masks/");
            if (maskDataPath.exists()) {
                String[] fileList = maskDataPath.list();
                for (String pathname : fileList) {
                    cropped.add(new Cropped(maskDataPath.getAbsolutePath() + "/" + pathname));
                }
            }








            //
            // Show values
            //
            Double ratio = traits.getJSONObject("27").getDouble("value");
            ratioValue.setText(df.format(ratio));
            Double peduncleSize = traits.getJSONObject("31").getDouble("value");
            peduncleSizeValue.setText(floored.format(peduncleSize));
            Double vol = traits.getJSONObject("102").getDouble("value");
            volumeValue.setText(floored.format(vol / 1000));
            distanceValue.setText(floored.format(variety.getDouble("distance")));

//            Double width = traits.getJSONObject("34_diameter").getDouble("value");
//            widthValue.setText(floored.format(width));
//            Double height = traits.getJSONObject("101_height").getDouble("value");
//            heightValue.setText(floored.format(height));


            this.croppedList = cropped;
            adapter = new CroppedAdapter(this.croppedList);
            this.cropped.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

}
