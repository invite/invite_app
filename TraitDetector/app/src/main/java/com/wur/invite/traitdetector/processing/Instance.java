package com.wur.invite.traitdetector.processing;



import android.media.Image;
import android.util.Log;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

public class Instance {

    // For tomato
    public int c;
    public float[] b;
    float s;
    public float[] m;
    public Mat m_map;
    public int orientation; // 0=top, 1=side, 2=bottom
    int[] centroid;
    float width;
    float widthMm;
    float height;
    float heightMm;

    // For peduncles
    public int c2;
    public float[] b2;
    float s2;
    public float[] m2;
    public Mat m_map2;
    float width_2;
    float widthMm_2;
    float height_2;
    float heightMm_2;


    public int index;
    float distance;
    Mat img;
    Mat img_output;



    public Instance(int c, float[] b, float s, float[] m) {
        this.c = c;
        this.b = b;
        this.s = s;
        this.m = m;

        int c_x = (int) ((b[2] + b[0])/2);
        int c_y = (int) ((b[3] + b[1])/2);

        this.centroid = new int[]{c_x, c_y};
    }













    public int get_centroid_x() {
        return centroid[0];
    }

    //    public void calculateDimensions(float pixPerMmX, float pixPerMmY) {
//    public void calculateDimensions(int pixelStride, int rowStride, ByteBuffer depthBufferRaw, float[] fovS, int[] img_shape, int[] depth_shape) throws IOException {
//        this.centroid = new int[]{(int) ((this.b[0] + this.b[2]) / 2), (int) ((this.b[1] + this.b[3]) / 2)};
//
//        float depth_scale_x = img_shape[0] / depth_shape[0];
//        float depth_scale_y = img_shape[1] / depth_shape[1];
//
//        int x = (int) (this.centroid[0] / depth_scale_x);
//        int y = (int) (this.centroid[1] / depth_scale_y);
//
//        int depthIndex = x * pixelStride + y * rowStride;
//
//        ByteBuffer depthBuffer = ByteBuffer.allocate(depthBufferRaw.capacity());
//        depthBufferRaw.rewind();
//        depthBuffer.put(depthBufferRaw);
//        depthBufferRaw.rewind();
//        depthBuffer.flip();
//
//        this.distance = depthBufferRaw.getShort(depthIndex);
//
////        float[] distance = getDepthValue(depthImage, depthConfidence, (int) this.centroid[0], (int) this.centroid[1]);
////        int distance = 1;
//
//        // Calculate the image width in mm using the field of view
//        float Tx = (float) (2 * Math.tan(0.5 * fovS[0]) * this.distance);
//        float Ty = (float) (2 * Math.tan(0.5 * fovS[1]) * this.distance);
//        float pixPerMmX = Tx / img_shape[0]; //#############
//        float pixPerMmY = Ty / img_shape[1];
//
//        //
//        // [1] Get tomato width and height
//        //
//        float[] dimensions1 = ImageOps.get_object_dimensions(this.m_map);
//        this.width = dimensions1[0];
//        this.height =  dimensions1[1];
//
//        //
//        // [2] Get peduncle width and height
//        //
//        if (this.c2 == 1)
//        {
//            float[] dimensions2 = ImageOps.get_object_dimensions(this.m_map2);
//            this.width_2 = dimensions2[0];
//            this.height_2 =  dimensions2[1];
//
//            this.widthMm_2 = this.width_2 * pixPerMmX;
//            this.heightMm_2 = this.height_2 * pixPerMmY;
//        }
//        else{
//            this.width_2 = 0;
//            this.height_2 = 0;
//            this.widthMm_2 = 0;
//            this.heightMm_2 = 0;
//        }
//
//        this.widthMm = this.width * pixPerMmX;
//        this.heightMm = this.height * pixPerMmY;
//    }




}