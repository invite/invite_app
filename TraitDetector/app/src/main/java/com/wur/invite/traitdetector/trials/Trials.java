package com.wur.invite.traitdetector.trials;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;
import android.os.Environment;

import com.wur.invite.traitdetector.gallery.Tomato;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Trials {
    private static JSONArray trialData;
    static String picturesPath;
    static File trialDataPath;







    public static ArrayList<Tomato> initTrialData(Context context) throws JSONException, ParseException {
        ArrayList<Tomato> tomatoes = new ArrayList<>();

        picturesPath = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES).toString();
        trialDataPath = new File(picturesPath + "/Invite/");

        if (!trialDataPath.exists()) {
            trialDataPath.mkdirs();
        }

        trialData = loadTrialData(context);

        for (int i = 0; i < trialData.length(); i++) {
            JSONObject variety = trialData.getJSONObject(i);
            String label = variety.getString("label");
            String date = variety.getString("date");
            int id = variety.getInt("id");

            String rawImgPath = getTrialRawImg(variety);
            tomatoes.add(new Tomato(label, id, date, rawImgPath));
        }

        return tomatoes;
    }



    public static String getTrialRawImg(JSONObject variety) throws JSONException, ParseException {
        String date = variety.getString("date");
        String trial = variety.getString("trial");
        int id = variety.getInt("id");

        SimpleDateFormat input = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date extracted_date = input.parse(date);
        SimpleDateFormat output = new SimpleDateFormat("yyyyMMddHHmm");
        String date_for_filename = output.format(extracted_date);
        String varietyCode = date_for_filename + "_" + trial + "_" + id;

        String rawImgPath = trialDataPath.getAbsolutePath() + "/" + varietyCode + "/raw.png";
        return rawImgPath;
    }


    public static String getTrialOutputImg(JSONObject variety) throws JSONException, ParseException {
        String date = variety.getString("date");
        String trial = variety.getString("trial");
        int id = variety.getInt("id");

        SimpleDateFormat input = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date extracted_date = input.parse(date);
        SimpleDateFormat output = new SimpleDateFormat("yyyyMMddHHmm");
        String date_for_filename = output.format(extracted_date);
        String varietyCode = date_for_filename + "_" + trial + "_" + id;

        String outputImgPath = trialDataPath.getAbsolutePath() + "/" + varietyCode + "/output.png";
        return outputImgPath;
    }

    public static void saveTrialData(Context context, JSONArray trialData){
        try {
            FileOutputStream fileout = context.openFileOutput("trialData.json", MODE_PRIVATE);
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileout);
            outputWriter.write(trialData.toString());
            outputWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static JSONArray loadTrialData(Context context) {
        JSONArray trialData = new JSONArray();
//        File picturesPath = Environment.getExternalStoragePublicDirectory(
//                Environment.DIRECTORY_PICTURES);
//        File trialDataPath = new File(picturesPath + "/Invite/");
//        File trialDataFile = new File(trialDataPath.getAbsolutePath() + "/trialData.json");

        File trialDataFile = new File(context.getFilesDir().getPath() + "/trialData.json");

        if (trialDataFile.exists()) {
            try {
                FileInputStream fileIn = context.openFileInput("trialData.json");
                InputStreamReader InputRead = new InputStreamReader(fileIn);

                char[] inputBuffer = new char[100];
                String s = "";
                int charRead;

                while ((charRead = InputRead.read(inputBuffer)) > 0) {
                    // char to string conversion
                    String readstring = String.copyValueOf(inputBuffer, 0, charRead);
                    s += readstring;
                }
                InputRead.close();
                trialData = new JSONArray(s);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return trialData;
    }
}
