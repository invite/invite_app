package com.wur.invite.traitdetector.processing;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.wur.invite.traitdetector.config.Config;
import com.wur.invite.traitdetector.depth.DepthData;
import com.wur.invite.traitdetector.processing.models.SRGan;
import com.wur.invite.traitdetector.processing.models.YOLOv8;
import com.wur.invite.traitdetector.processing.models.YOLOv8_2;

import org.json.JSONException;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Processor {

    public static List<Instance> exclude_outliers(List<Instance> preds, int threshold){
        List<Instance> filtered_preds = new ArrayList<>();
        ArrayList<Integer> centroidsX = new ArrayList<>();
        ArrayList<Integer> centroidsY = new ArrayList<>();

        for (Instance p : preds) {
            centroidsX.add(p.centroid[0]);
            centroidsY.add(p.centroid[1]);
        }
        int ave_centroidX = (int) centroidsX.stream().mapToInt(a -> a).average().orElse(-1);
        int ave_centroidY = (int) centroidsY.stream().mapToInt(a -> a).average().orElse(-1);

        int i = 0;
        for (Instance p : preds) {
            int x1 = centroidsX.get(i);
            int y1 = centroidsY.get(i);
            int eu_distance = ImageOps.euclidean_distance(x1, y1, ave_centroidX, ave_centroidY);
            if (eu_distance <= threshold)
                filtered_preds.add(p);
            i = i + 1;
        }

        return filtered_preds;
    }

    public static List<Instance> assign_orientations(List<Instance> preds){
        ArrayList<Integer> centroidsY = new ArrayList<>();
        for (Instance p : preds) {
            centroidsY.add(p.centroid[1]);
        }
        int ave_centroidY = (int) centroidsY.stream().mapToInt(a -> a).average().orElse(-1);

        List<Instance> top_objects = new ArrayList<>();
        List<Instance> mid_objects = new ArrayList<>();
        List<Instance> bot_objects = new ArrayList<>();
        for (Instance p : preds) {
            int c_y = p.centroid[1];
            int difference = ave_centroidY - c_y;
            float p_difference = ((float) Math.abs(ave_centroidY - c_y) / (float) ave_centroidY)*100.00f;
            if ((difference < 0) && (p_difference > 15))
            {
                p.orientation = 2;
                bot_objects.add(p);
            }
            else if ((difference > 0) && (p_difference > 15))
            {
                p.orientation = 0;
                top_objects.add(p);
            }
            else if (p_difference < 15)
            {
                p.orientation = 1;
                mid_objects.add(p);
            }

        }
        top_objects.sort(Comparator.comparing(Instance::get_centroid_x));
        mid_objects.sort(Comparator.comparing(Instance::get_centroid_x));
        bot_objects.sort(Comparator.comparing(Instance::get_centroid_x));

        List<Instance> all_objects = new ArrayList<>();
        all_objects.addAll(top_objects);
        all_objects.addAll(mid_objects);
        all_objects.addAll(bot_objects);

        int i = 1;
        for (Instance p : all_objects) {
            p.index = i;
            i=i+1;
        }

        return all_objects;
    }

    public static Mat conditional_upscaling(Mat img, int threshold, int target_size){
        Mat sr_img = new Mat();
        int h = img.height();
        int w = img.width();
        if (h < threshold || w <threshold)  sr_img = SRGan.process_image(img);
        else  Imgproc.resize(img, sr_img, new Size(target_size, target_size));
        return sr_img;
    }

    public static float[] pixel_to_mm(Mat img, Mat cropped_img, Mat mask, DepthData depth_data, float[] fov){
        float pixels_to_mm_X = 0;
        float pixels_to_mm_Y = 0;
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

        mask.convertTo(mask, CvType.CV_32SC1, 255);
        Imgproc.findContours(mask, contours, new Mat(), Imgproc.RETR_FLOODFILL, Imgproc.CHAIN_APPROX_SIMPLE);
        Point[] max_contour = contours.get(ImageOps.get_max_contour(contours)).toArray();
        MatOfPoint2f areaPoints = new MatOfPoint2f(max_contour);
        RotatedRect boundingRect = Imgproc.minAreaRect(areaPoints);

        float rw = 0;
        float rh = 0;
        if (boundingRect.angle == 90)
        {
            rw = (float)boundingRect.size.width;
            rh = (float)boundingRect.size.height;
        }
        else
        {
            rw = (float)boundingRect.size.height;
            rh = (float)boundingRect.size.width;
        }

        pixels_to_mm_X = (float) (((2.0f * Math.tan(0.5f * fov[0])) * depth_data.distance) / img.width());
        pixels_to_mm_Y = (float) (((2.0f * Math.tan(0.5f * fov[1])) * depth_data.distance) / img.height());


        float w_mm = rw * pixels_to_mm_X;
        float h_mm = rh * pixels_to_mm_Y;

        return new float[]{w_mm, h_mm};
    }

    public static List<Instance> full_process_image(String filename, Mat img, Config user_cfg, DepthData depth_data, float[] fov, Context context) throws IOException, JSONException {
        long startTime = 0;
        double duration = 0;
        long mainStartTime = System.nanoTime();
        int[] img_shape = {img.width(), img.height()};


        // Detect tomatoes
        startTime = System.nanoTime();
        List<Instance> preds_stage1  = YOLOv8.process_image(img);
        duration = (System.nanoTime() - startTime)/1000000.0f;
        Log.d("INVITE", "[YOLO1]" + String.valueOf(duration) + " n:" + preds_stage1.size());

        // Exclude outliers based on centroid
        preds_stage1 = exclude_outliers(preds_stage1, 480);

        // Assign orientations
        List<Instance> all_objects = assign_orientations(preds_stage1);


        if (user_cfg.pipeline == 1) {
        }

        // If using camera distance only
        int obj_distance = 0;
        if (depth_data.distance != 0) {
            obj_distance = depth_data.distance;
        }


        if (user_cfg.pipeline == 2) {
            for (Instance o : all_objects) {
                float[] b = o.b;
                int x1 = (int) (b[0] * 0.98f);
                int y1 = (int) (b[1] * 0.98f);
                int x2 = (int) (b[2] * 1.02f);
                int y2 = (int) (b[3] * 1.02f);
                Rect roi = new Rect((int) (x1), (int) (y1), (int) (x2 - x1), (int) (y2 - y1));
                Mat cropped_img = new Mat(img, roi);
                int w = cropped_img.width();
                int h = cropped_img.height();
                Mat m_map = o.m_map;


                startTime = System.nanoTime();
                Mat sr_img = conditional_upscaling(cropped_img, user_cfg.sr_threshold, user_cfg.sr_force_size);
                duration = (System.nanoTime() - startTime)/1000000.0f;
                Log.d("INVITE", "[SR   ]" + String.valueOf(duration));

                startTime = System.nanoTime();
                List<Instance> preds_stage2  = YOLOv8_2.process_image(sr_img);
                duration = (System.nanoTime() - startTime)/1000000.0f;
                Log.d("INVITE", "[YOLO2]" + String.valueOf(duration) + " n:" + preds_stage2.size());




                // Postprocess new tomato masks and peduncles
                for (Instance p : preds_stage2) {
                    float[] b2 = p.b;
                    int c2 = p.c;
                    float s2 = p.s;
                    Mat m_map2 = p.m_map;

                    // Add distance info`
                    if (depth_data.depth_shape[0] != 0){
                        depth_data.compute_distance(o.centroid, img_shape);
                        Log.d("INVITE", "[ARCore] distance = " + depth_data.distance);
                    }
                    // For debug only
                    else{
                        depth_data.distance = obj_distance;
                        Log.d("INVITE", "[ARCore] distance = " + obj_distance);
                    }
                    o.distance = depth_data.distance;



                    // If tomato, replace previous mask with improved mask
                    if (c2 == 0) {
                        m_map2.convertTo(m_map2, CvType.CV_8U, 255);

                        // Resize map according to original tomato pixel size
                        Imgproc.resize(m_map2, m_map2, new Size(w, h), Imgproc.INTER_CUBIC);
                        m_map2.copyTo(m_map.submat(y1, y2, x1, x2));
                        o.m_map = m_map;


                        float[] mm_dims = pixel_to_mm(img, cropped_img, m_map2, depth_data, fov);
                        float w_mm = mm_dims[0];
                        float h_mm = mm_dims[1];
                        o.widthMm = w_mm;
                        o.heightMm = h_mm;
                    }

                    // If peduncle
                    if (c2 == 1  && o.orientation == 0) {
                        Mat clone_mat = new Mat(img_shape[1], img_shape[0],  CvType.CV_8U, Scalar.all(0));
                        Mat dest_mat = clone_mat.submat(y1, y2, x1, x2);
                        m_map2.convertTo(m_map2, CvType.CV_8U, 255);

                        // Resize map according to original tomato pixel size
                        Imgproc.resize(m_map2, m_map2, new Size(w, h), Imgproc.INTER_CUBIC);
                        m_map2.copyTo(dest_mat);
                        o.c2 = c2;
                        o.s2 = s2;
                        o.m_map2 = clone_mat;
                        o.b2 = b2;

                        float[] mm_dims = pixel_to_mm(img, cropped_img, m_map2, depth_data, fov);
                        float w_mm = mm_dims[0];
                        float h_mm = mm_dims[1];
                        o.widthMm_2 = w_mm;
                        o.heightMm_2 = h_mm;
                    }
                }
                Imgproc.cvtColor(sr_img, sr_img, Imgproc.COLOR_BGR2RGB);
                o.img = sr_img;

            }
        }



        double mainDuration = (System.nanoTime() - mainStartTime)/1000000.0f;
        Log.d("INVITE", "Total processing time:" + String.valueOf(mainDuration));


        COCO.write_coco_json(filename, img, all_objects, context);


        return all_objects;
    }
















    public static void extract_instances(List<Instance> instancesList, File masksPath) throws IOException {
        if (!masksPath.exists()) {
            masksPath.mkdirs();
        }

        int id = 0;
        for (Instance instance : instancesList) {

            // Get output image
            Mat img_output =  instance.img;

            // Mat to bitmap
            Bitmap bitmap_output = null;
            bitmap_output = Bitmap.createBitmap(img_output.width(), img_output.height(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(img_output, bitmap_output);

            // Save file
            final File out = new File(masksPath.getAbsolutePath(), id + ".png");
            FileOutputStream fos = new FileOutputStream(out);
            bitmap_output.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();

            id = id + 1;
        }
    }
}
