package com.wur.invite.traitdetector.gallery;

public class Tomato {

    private String name;
    private int id;
    private String imageURI;
    public String date;

    public Tomato(String name, int id, String date, String imageURI) {
        this.name = name;
        this.id = id;
        this.date = date;
        this.imageURI = imageURI;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageURI() {
        return imageURI;
    }

    public void setImageURI(String imageURI) {
        this.imageURI = imageURI;
    }
}
