package com.wur.invite.traitdetector.qr;

import static java.lang.Integer.parseInt;

import android.media.Image;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.barcode.BarcodeScanner;
import com.google.mlkit.vision.barcode.BarcodeScannerOptions;
import com.google.mlkit.vision.barcode.BarcodeScanning;
import com.google.mlkit.vision.barcode.common.Barcode;
import com.google.mlkit.vision.common.InputImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class QrCodeHandler {

    BarcodeScannerOptions options = new BarcodeScannerOptions.Builder()
            .setBarcodeFormats(
                    Barcode.FORMAT_QR_CODE,
                    Barcode.FORMAT_AZTEC)
            .build();
    BarcodeScanner scanner = BarcodeScanning.getClient();
    String[] rawValue;
    JSONObject qrJSON = new JSONObject();

    public JSONObject defaultValues() throws JSONException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        Random rand = new Random();
        qrJSON.put("year", "2023");
        qrJSON.put("date", formatter.format(date));
        qrJSON.put("trial", "0");
        qrJSON.put("id", rand.nextInt(100000));
        qrJSON.put("label", "Random");
        JSONObject traits = new JSONObject();
        qrJSON.put("traits", traits);
        return qrJSON;
    }

    ;


    public JSONObject analyze(Image mediaImage) {
        InputImage image = InputImage.fromMediaImage(mediaImage, 0);

        Task<List<Barcode>> result = scanner.process(image)
                .addOnSuccessListener(new OnSuccessListener<List<Barcode>>() {
                    @Override
                    public void onSuccess(List<Barcode> barcodes) {
                        for (Barcode barcode : barcodes) {

                            rawValue = barcode.getRawValue().split("_");
                            try {

                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                                Date date = new Date();
                                qrJSON.put("date", formatter.format(date));

                                qrJSON.put("year", parseInt(rawValue[0]));
                                qrJSON.put("trial", parseInt(rawValue[1]));
                                qrJSON.put("id", parseInt(rawValue[2]));
                                qrJSON.put("label", rawValue[3]);

                                JSONObject reference = new JSONObject();
                                for (int i=4; i<rawValue.length; i+=2) {
                                    JSONObject trait = new JSONObject();
                                    trait.put("score", parseInt(rawValue[i+1]));
                                    reference.put(rawValue[i], trait);
                                }

                                qrJSON.put("traits", reference);

                                Log.i("BARCODE", String.valueOf(qrJSON));


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Task failed with an exception
                        // ...
                    }
                });

        return qrJSON;
    }
}
